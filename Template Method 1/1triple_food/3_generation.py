# -*- coding: utf-8 -*-
"""
@author: Pei Yu
"""
import pandas as pd
import re
path = 'C:/Ddrive/Master/Semantic Web Technology/Final Project/1triple_food'

template_df = pd.read_csv(path+'/template_df.csv')
y_pred = pd.read_csv(path+'/y_pred.csv')
X_test_df = pd.read_csv(path+'/X_test_df.csv')

# make a copy
y_sentence = y_pred

# generate
template_holder = []
for i in range(y_sentence.shape[0]):
    candidates= template_df.loc[template_df['Predicate'] == y_sentence.iloc[i][1]]['Template']
    # sample one template
    candidates= candidates.sample(n=1) 
    template_holder.append(candidates)

# flatten the list of list    
template_holder = [item for sublist in template_holder for item in sublist]
y_sentence['Template'] = template_holder

# copy triple columns 
y_sentence['Subject'] = X_test_df['Subject'].values
y_sentence['Predicate'] = X_test_df['Predicate'].values
y_sentence['Object'] = X_test_df['Object'].values

# reorder and drop unwanted columns
y_sentence = y_sentence[['Triple', 'Subject', 'Predicate', 'Object', 'Prediction', 'Template']]

#%%
# mapping
y_sentence['Sentence'] = y_sentence.apply(lambda x : x['Template'].replace('<SUBJECT>',(x['Subject'])),1)
y_sentence['Sentence'] = y_sentence.apply(lambda x : x['Sentence'].replace('<PREDICATE>',(x['Predicate'])),1)
y_sentence['Sentence'] = y_sentence.apply(lambda x : x['Sentence'].replace('<OBJECT>',(x['Object'])),1)

# reorder and drop unwanted columns
y_sentence = y_sentence[['Triple', 'Prediction', 'Template', 'Sentence']]

export_csv = y_sentence.to_csv (path+'\y_sentence.csv', index = None, header=True)




