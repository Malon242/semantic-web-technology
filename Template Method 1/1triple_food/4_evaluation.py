# -*- coding: utf-8 -*-
"""
@author: Pei Yu
"""
import pandas as pd
import re
path = 'C:/Ddrive/Master/Semantic Web Technology/Final Project/1triple_food'
Y_test_df = pd.read_csv(path+'/Y_test_df.csv')
y_sentence = pd.read_csv(path+'/y_sentence.csv')

#%%%%%%%%%%%%%%%%%%%%% Evaluation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
from nltk.translate.bleu_score import sentence_bleu
sen = []
ans = []
bleu1=[]
bleu4=[]
for i in range(0,len(y_sentence)):
    candidate = y_sentence.loc[i]['Sentence']
    candidate = re.sub("[^a-zA-Z0-9]+", " ", candidate)
    candidate = candidate.lower()
    candidate_token = candidate.split()
  
    # get its corresponding triple to get the 'answer'
    triple = y_sentence.loc[i]['Triple']
    reference = Y_test_df.loc[Y_test_df['Triple'] == y_sentence.loc[i]['Triple']]['Sentence']
    reference = reference.tolist()    
    reference_token = reference
    # split 
    for j in range(0, len(reference_token)):
        reference_token[j] = re.sub("[^a-zA-Z0-9]+", " ", reference_token[j])
        reference_token[j] = reference_token[j].lower()
        reference_token[j] = reference_token[j].split()
    
    BLEU_1 = sentence_bleu(reference_token, candidate_token, weights=(1, 0, 0, 0))
    BLEU_4 = sentence_bleu(reference_token, candidate_token, weights=(0.25, 0.25, 0.25, 0.25))
    
    bleu1.append(BLEU_1)
    bleu4.append(BLEU_4)
    
    sen.append(candidate)
    ans.append(reference)
    
    
#%% make a df
#sen = pd.DataFrame(sen, columns=['Sentence'])
eval_result = pd.DataFrame()
eval_result['Triple'] = y_sentence['Triple']
eval_result['Sentence'] = sen
eval_result['BLEU_1'] = bleu1
eval_result['BLEU_4'] = bleu4

# how many candidates
n = Y_test_df.groupby('Triple')['Sentence'].nunique()
n = n.tolist()
eval_result['n'] = n

# repeat row n times
eval_result = eval_result.reindex(eval_result.index.repeat(eval_result.n)).reset_index(drop=True)

# attach 'answer'
eval_result['Answer'] = Y_test_df['Sentence']

# reorder and drop
eval_result= eval_result[['Triple', 'Sentence', 'Answer',
                          'BLEU_1', 'BLEU_4']]
export_csv = eval_result.to_csv (path+'\eval_result.csv', index = None, header=True)

