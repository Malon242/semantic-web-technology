# -*- coding: utf-8 -*-
"""
@author: Pei Yu
"""
from bs4 import BeautifulSoup as bs
import itertools
import os
import re
#%%%%%%%%%% Data Preparation #%%%%%%%%%%
#### read training data ####
path = 'C:/Ddrive/Master/Semantic Web Technology/Final Project/1triple_food'
INPUT_TRAIN = path+"/clean_1triple_food_train.xml"

# read the file part adopt from 
# https://github.com/lucas0/WebNLG/blob/master/Train/generate_xy%2Bw2v_model.py 
X_train =[]
Y_train =[]

with open(INPUT_TRAIN, "r+") as f:
	soup = bs(f, "lxml")
	# entry = entry
	# mtriple = triples
	# lex = sentence
	for elem in soup.findAll('entry'):
		elem_id = elem.get('eid')
		triples = [e.string.encode('ascii', 'ignore') for e in elem.findAll('mtriple')]
		sentences = [e.string.encode('ascii', 'ignore') for e in elem.findAll('lex')]
		
		for e in list(itertools.product(triples,sentences)):
			X_train.append(e[0])
			Y_train.append(e[1])
            
#### read dev data ####
INPUT_DEV = path+"/clean_1triple_food_dev.xml"

X_dev =[]
Y_dev =[]

with open(INPUT_DEV, "r+") as f:
	soup = bs(f, "lxml")
	# entry = entry
	# mtriple = triples
	# lex = sentence
	for elem in soup.findAll('entry'):
		elem_id = elem.get('eid')
		triples = [e.string.encode('ascii', 'ignore') for e in elem.findAll('mtriple')]
		sentences = [e.string.encode('ascii', 'ignore') for e in elem.findAll('lex')]
		
		for e in list(itertools.product(triples,sentences)):
			X_dev.append(e[0])
			Y_dev.append(e[1])

#### read testing data ####
INPUT_TEST =  path+"/clean_1triple_food_test.xml"

X_test =[]
Y_test =[]

with open(INPUT_TEST, "r+") as f:
	soup = bs(f, "lxml")
	# entry = entry
	# mtriple = triples
	# lex = sentence
	for elem in soup.findAll('entry'):
		elem_id = elem.get('eid')
		triples = [e.string.encode('ascii', 'ignore') for e in elem.findAll('mtriple')]
		sentences = [e.string.encode('ascii', 'ignore') for e in elem.findAll('lex')]
		
		for e in list(itertools.product(triples,sentences)):
			X_test.append(e[0])
			Y_test.append(e[1])

#%% Preprocessing train, dev, test            
# convert bytes to string, and remove special character ONLY IN X
for i in range(0,len(X_train)):
    X_train[i] = X_train[i].decode() 
    X_train[i] = re.sub("[^a-zA-Z0-9|.,]+", " ", X_train[i])
for i in range(0,len(Y_train)):
    Y_train[i] = Y_train[i].decode()

    
for i in range(0,len(X_dev)):
    X_dev[i] = X_dev[i].decode() 
    X_dev[i] = re.sub("[^a-zA-Z0-9|.,]+", " ", X_dev[i])
for i in range(0,len(Y_dev)):
    Y_dev[i] = Y_dev[i].decode()


for i in range(0,len(X_test)):
    X_test[i] = X_test[i].decode()    
    X_test[i] = re.sub("[^a-zA-Z0-9|.,]+", " ", X_test[i])
for i in range(0,len(Y_test)):
    Y_test[i] = Y_test[i].decode()



#%% Divide triple into three columns
import pandas as pd
# put into df
X_train_df = pd.DataFrame ({'Triple':X_train})
Y_train_df = pd.DataFrame ({'Sentence':Y_train})

X_dev_df = pd.DataFrame ({'Triple':X_dev})
Y_dev_df = pd.DataFrame ({'Sentence':Y_dev})

X_test_df = pd.DataFrame ({'Triple':X_test})
Y_test_df = pd.DataFrame ({'Sentence':Y_test})

# combine train and dev
X_train_df = pd.concat([X_train_df, X_dev_df], axis=0)
Y_train_df = pd.concat([Y_train_df, Y_dev_df], axis=0)

# remove special character
# X_train_df['Triple'] = X_train_df['Triple'].str.replace('_',' ')
# X_train_df['Triple'] = X_train_df['Triple'].str.replace('"','')
# X_train_df['Triple'] = X_train_df['Triple'].str.replace('~','')

# split into three columns
X_train_df[['Subject', 'Predicate', 'Object']] = X_train_df['Triple'].str.split('|',expand=True)
X_test_df[['Subject', 'Predicate', 'Object']] = X_test_df['Triple'].str.split('|',expand=True)

# remove white space
X_train_df['Subject'] = X_train_df['Subject'].str.strip()
X_train_df['Predicate'] = X_train_df['Predicate'].str.strip()
X_train_df['Object'] = X_train_df['Object'].str.strip()

X_test_df['Subject'] = X_test_df['Subject'].str.strip()
X_test_df['Predicate'] = X_test_df['Predicate'].str.strip()
X_test_df['Object'] = X_test_df['Object'].str.strip()

# concat to one df to build the templates

template_df = pd.concat([X_train_df, Y_train_df], axis=1)

#lowercase
template_df = template_df.apply(lambda x: x.astype(str).str.lower())

# build
template_df['Template'] = template_df.apply(lambda x : x['Sentence'].replace((x['Object']),'<OBJECT>'),1)
template_df['Template'] = template_df.apply(lambda x : x['Template'].replace((x['Subject']),'<SUBJECT>'),1)
template_df['Template'] = template_df.apply(lambda x : x['Template'].replace((x['Predicate']),'<PREDICATE>'),1)

# =============================================================================
# # then I need to check manually, because of several reasons:
# # typo
# # 有些subject<object，或反過來，會重複取代
# # not completely match
# =============================================================================

export_csv = template_df.to_csv (path+'/template_df.csv', index = None, header=True)

# concat X test and Y test for BLEU score
Y_test_df = pd.concat([X_test_df, Y_test_df], axis=1)

# delete duplicate rows in test X
X_test_df = X_test_df.drop_duplicates()

export_csv = X_test_df.to_csv (path+'/X_test_df.csv', index = None, header=True)
export_csv = Y_test_df.to_csv (path+'/Y_test_df.csv', index = None, header=True)











    
    
