# -*- coding: utf-8 -*-
"""
@author: Pei Yu
"""
import os
import pandas as pd
import numpy as np
path = 'C:/Ddrive/Master/Semantic Web Technology/Final Project/1triple_food'

template_df = pd.read_csv(path+'/template_df.csv')
X_test_df = pd.read_csv(path+'/X_test_df.csv')

# select useful columns so it's easier to look
# feala_df = template_df[['Predicate','Template']]
# feature_label_df['Label'] = (feature_label_df['Template']).astype('category').cat.codes

#%%%%%%%%%%%%% word embedding: gloVe %%%%%%%%%%%%%%

### training data
docs = template_df['Predicate'].tolist()

import string
docs = [''.join(c for c in s if c not in string.punctuation) for s in docs]

from keras.preprocessing.text import Tokenizer
# prepare tokenizer
t = Tokenizer()
t.fit_on_texts(docs)
vocab_size = len(t.word_index) + 1
print(t.word_index)

# integer encode the documents
encoded_docs = t.texts_to_sequences(docs)
#print(encoded_docs)
#max(encoded_docs,key=len)

from keras.preprocessing.sequence import pad_sequences
# pad documents to  max length 
max_length = len(max(encoded_docs,key=len))
padded_docs = pad_sequences(encoded_docs, maxlen=max_length, padding='post')
#print(padded_docs)


# the actual embed part
# load the whole embedding into memory
embeddings_index = dict()
f = open('C:\Ddrive\Master\Language Technology Project\Project\glove.6B.100d.txt', encoding='utf-8')

for line in f:
	values = line.split()
	word = values[0]
	coefs = np.asarray(values[1:], dtype='float32')
	embeddings_index[word] = coefs
f.close()
print('Loaded %s word vectors.' % len(embeddings_index))

# create a weight matrix for words in training docs
embedding_matrix = np.zeros((vocab_size, 100))
for word, i in t.word_index.items():
	embedding_vector = embeddings_index.get(word)
	if embedding_vector is not None:
		embedding_matrix[i] = embedding_vector
        
x_vec_train = []
for a in docs:
    words = a.split(' ')
    vector = np.zeros((100))
    word_num = 0
    for b in words:
        if str(b).lower() in embeddings_index:
            vector += embeddings_index[str(b).lower()]
            word_num += 1
    if word_num > 0:
        vector = vector/word_num
    x_vec_train.append(vector)    

x_vec_train = pd.DataFrame(x_vec_train)

########## do the same for test data ##########
docs = X_test_df['Predicate'].tolist()

import string
docs = [''.join(c for c in s if c not in string.punctuation) for s in docs]

from keras.preprocessing.text import Tokenizer
# prepare tokenizer
t = Tokenizer()
t.fit_on_texts(docs)
vocab_size = len(t.word_index) + 1
print(t.word_index)

# integer encode the documents
encoded_docs = t.texts_to_sequences(docs)
#print(encoded_docs)
#max(encoded_docs,key=len)

from keras.preprocessing.sequence import pad_sequences
# pad documents to a max length of 4 words
max_length = len(max(encoded_docs,key=len))
padded_docs = pad_sequences(encoded_docs, maxlen=max_length, padding='post')
#print(padded_docs)


# the actual embed part
# load the whole embedding into memory
embeddings_index = dict()
f = open('C:\Ddrive\Master\Language Technology Project\Project\glove.6B.100d.txt', encoding='utf-8')

for line in f:
	values = line.split()
	word = values[0]
	coefs = np.asarray(values[1:], dtype='float32')
	embeddings_index[word] = coefs
f.close()
print('Loaded %s word vectors.' % len(embeddings_index))

# create a weight matrix for words in training docs
embedding_matrix = np.zeros((vocab_size, 100))
for word, i in t.word_index.items():
	embedding_vector = embeddings_index.get(word)
	if embedding_vector is not None:
		embedding_matrix[i] = embedding_vector

x_vec_test = []
for a in docs:
    words = a.split(' ')
    vector = np.zeros((100))
    word_num = 0
    for b in words:
        if str(b).lower() in embeddings_index:
            vector += embeddings_index[str(b).lower()]
            word_num += 1
    if word_num > 0:
        vector = vector/word_num
    x_vec_test.append(vector)    

x_vec_test = pd.DataFrame(x_vec_test)

#%%%%%%%%%%%%%%% build a classfier to choose template %%%%%%%%%%%%%%%%%%%%%%%%
# reduce dimension

# fit train
from sklearn.decomposition import PCA
pca = PCA(n_components=0.95)
x_vec_train_pca = pca.fit_transform(x_vec_train)
x_vec_train_pca = pd.DataFrame(x_vec_train_pca)

pca.explained_variance_ratio_.cumsum()

# fit test
x_vec_test_pca = pca.transform(x_vec_test) # don't fit
x_vec_test_pca = pd.DataFrame(x_vec_test_pca)
#%%
y_label_train = template_df['Predicate']

from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score, validation_curve

knn = KNeighborsClassifier(n_neighbors=1, metric='euclidean')
knn.fit(x_vec_train, y_label_train)
accuracy = cross_val_score(knn, x_vec_train, y_label_train, scoring='accuracy', cv = 5)
print(accuracy)
#get the mean of each fold 
print("Accuracy of Model with Cross Validation is:",accuracy.mean() * 100)

#%% validation curve, adopt frm sklearn documentation
# Create range of values for parameter
param_range = np.arange(1, 6)

# Calculate accuracy on training and test set using range of parameter values
train_scores, test_scores = validation_curve(KNeighborsClassifier(), 
                                             x_vec_train, y_label_train, 
                                             param_name="n_neighbors", 
                                             param_range=param_range,
                                             cv=5, 
                                             scoring="accuracy", 
                                             n_jobs=-1)


# Calculate mean and standard deviation for training set scores
train_mean = np.mean(train_scores, axis=1)
train_std = np.std(train_scores, axis=1)

# Calculate mean and standard deviation for test set scores
test_mean = np.mean(test_scores, axis=1)
test_std = np.std(test_scores, axis=1)

import matplotlib.pyplot as plt
# import matplotlib.pyplot as plt
# Plot mean accuracy scores for training and test sets
plt.plot(param_range, train_mean, label="Training score", color="black")
plt.plot(param_range, test_mean, label="Cross-validation score", color="dimgrey")

# Plot accurancy bands for training and test sets
plt.fill_between(param_range, train_mean - train_std, train_mean + train_std, color="gray")
plt.fill_between(param_range, test_mean - test_std, test_mean + test_std, color="gainsboro")

# Create plot
plt.title("Validation Curve With Random Forest")
plt.xlabel("Number Of Trees")
plt.ylabel("Accuracy Score")
plt.tight_layout()
plt.legend(loc="best")
plt.show()
# -->> k = 1 is the best
#%%
y_pred = knn.predict(x_vec_test)

# add Triple column for BLEU reference

y_pred = pd.DataFrame ({'Prediction':y_pred})
y_pred['Triple'] = X_test_df['Triple'].values
# reorder 
y_pred = y_pred[['Triple', 'Prediction']]

export_csv = y_pred.to_csv (path+'\y_pred.csv', index = None, header=True)











