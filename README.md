# Semantic Web Technology Project
This project demonstrates natural language generation from RDF Triples, using both template-based method and neural machine translation method. There are two different template methods and for neural machine translation we used OpneNMT. The codes for each methods are in their own folders. Below are the instructions to run the codes.

## Template Method 1
The entire process is divided into several files. Please run the ```.py``` files in order from 0 to 4 (clean ➔ preprocessing ➔ word embedding ➔ generation ➔ evaluation).

The ```.csv``` files and the **clean_xxx**```.xml``` files are the results of running the ```.py``` files.

The paths in the ```.py``` files may need to be adjusted depending on your local directory.

## Template Method 2
The entire process from preprocessing to evaluation is encompassed in a single file. Please run the ```swt_temp.py``` file.

The paths in ```swt_temp.py``` to the ```.xml``` data files might need to be adjusted according to your local directory.

## RDF2text
The entire process from preprocessing to evaluation is encompassed in a single file ```RUNALL.sh``` in the ```RDF2text``` directory. Please run the ```sh RUNALL.sh``` from within the RDF2text directory, since all paths provided in the script are relative to the RDF2text directory.
