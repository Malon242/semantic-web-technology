from sklearn.feature_extraction.text import CountVectorizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline

from nltk.translate.bleu_score import sentence_bleu
from SPARQLWrapper import SPARQLWrapper, JSON
from unidecode import unidecode
from operator import itemgetter

import xml.etree.cElementTree as ET
import pandas as pd
import nltk
import re


def getData():
	"""Create test and train dataframes"""
	xml_food1 = "../webnlg_challenge_2017-from-webnlg-dataset/train/1triples/1triple_allSolutions_Food_train_challenge.xml" 
	xml_food2 = "../webnlg_challenge_2017-from-webnlg-dataset/dev/1triples/1triple_allSolutions_Food_dev_challenge.xml" 
	xml_uni1 = "../webnlg_challenge_2017-from-webnlg-dataset/train/1triples/1triple_allSolutions_University_train_challenge.xml" 
	xml_uni2 = "../webnlg_challenge_2017-from-webnlg-dataset/dev/1triples/1triple_allSolutions_University_dev_challenge.xml" 
	xml_test = "../webnlg_challenge_2017-from-webnlg-dataset/test/testdata_with_lex.xml"

	df_food1 = createDF(ET.parse(xml_food1))
	df_food2 = createDF(ET.parse(xml_food2))
	df_uni1 = createDF(ET.parse(xml_uni1))
	df_uni2 = createDF(ET.parse(xml_uni2))
	train = pd.concat([df_food1, df_food2, df_uni1, df_uni2], axis=0)
	test = createDFtest(ET.parse(xml_test))

	return train, test


def createDF(xml):
	"""Create dataframe from xml"""
	cols = ['category', 'triple', 'lex']
	df = pd.DataFrame(columns=cols)
	
	for child in xml.getroot():
		for node in child:
			category = node.attrib.get('category')
			triple = node.find('modifiedtripleset/mtriple')
			lex = node.findall('lex[@comment="good"]')

			df = df.append(pd.Series(
				[category, getValues(triple), getValuesLst(lex)], 
				index=cols), ignore_index=True)

	return df


def createDFtest(xml):
	"""Create test dataframe from xml"""
	cols = ['category', 'triple', 'lex']
	df = pd.DataFrame(columns=cols)
	
	for entries in xml.getroot():
		for entry in entries.findall('.//entry[@size="1"]'):
			category = entry.attrib.get('category')
			triple = entry.find('modifiedtripleset/mtriple')
			lex = entry.findall('lex[@comment="good"]')

			if category == "Food" or category == "University":
				df = df.append(pd.Series(
					[category, getValues(triple), getValuesLst(lex)], 
					index=cols), ignore_index=True)

	return df


def getValues(node):
	"""Return text or None"""
	if node != None:
		return node.text
	else:
		return None


def getValuesLst(lst):
	"""Return values of list"""
	lst = ['{}'.format(item.text) for item in lst if len(lst)>0]
	return lst


def getSuperclass(subject):
	"""Create dataframe column with subject superclass"""
	subject = subject.replace("'", "\'")
	subject = subject.replace(",", "\,")

	try:
		sparql = SPARQLWrapper('http://dbpedia.org/sparql')
		sparql.setQuery("""
			PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX : <http://dbpedia.org/resource/>

			SELECT ?type WHERE {
			:"""+subject+""" rdf:type ?type.
			}LIMIT 10
		""")
		sparql.setReturnFormat(JSON)
		results = sparql.query().convert()

		types = []

		for results in results["results"]["bindings"]:
			types.append(results["type"]["value"])

		if "http://dbpedia.org/ontology/Person" in types:
			return "PERSON"
		elif "http://dbpedia.org/ontology/Satellite" in types:
			return "SATELLITE"
		elif "http://dbpedia.org/ontology/Award" in types:
			return "AWARD"
		elif "http://dbpedia.org/ontology/Food" in types:
			return "FOOD"
		elif "http://dbpedia.org/ontology/Activity" in types:
			return "ACTIVITY"
		elif "http://dbpedia.org/ontology/Work" in types:
			return "WORK"
		elif "http://dbpedia.org/ontology/Species" in types:
			return "SPECIES"
		elif "http://dbpedia.org/ontology/Organisation" in types:
			return "ORGANISATION"
		elif "http://dbpedia.org/ontology/Place" in types:
			return "PLACE"
		else:
			return "THING"

	except Exception as e:
		return "THING"


def replaceSubObj(row):
	"""Replace the subject, object and predicate in the sentences with placeholders"""
	row.subject = re.sub(r'\([^()]*\)', '', row.subject)
	row.object = re.sub(r'\([^()]*\)', '', row.object)

	new = []

	for sentence in row['lex']:
		# Predicate
		sentence3 = re.compile(' '.join(row.predicate.split(' '))+"*", re.IGNORECASE)
		sentence = sentence3.sub(r" <PREDICATE> ", sentence)
		row.predicate = unidecode(row.predicate)
		sentence3 = re.compile(' '.join(row.predicate.split(' '))+"*", re.IGNORECASE)
		sentence = sentence3.sub(r" <PREDICATE> ", sentence)

		# Object
		sentence2 = re.compile(' '.join(row.object.strip().split("_"))+"*", re.IGNORECASE)
		sentence = sentence2.sub(r" <OBJECT> ", sentence)
		row.object = re.sub(r'"', '', row.object)
		sentence2 = re.compile(' '.join(row.object.strip().split("_"))+"*", re.IGNORECASE)
		sentence = sentence2.sub(r" <OBJECT> ", sentence)
		sentence = sentence.replace(' '.join(row.object.split("_")), " <OBJECT> ")
		row.object = unidecode(row.object)
		sentence2 = re.compile(' '.join(row.object.strip().split("_"))+"*", re.IGNORECASE)
		sentence = sentence2.sub(r" <OBJECT> ", sentence)

		# Subject
		sentence1 = re.compile(' '.join(row.subject.strip().split("_"))+"*", re.IGNORECASE)
		sentence = sentence1.sub(r" <SUBJECT> ", sentence)
		row.subject = re.sub(r'"', '', row.subject)
		sentence1 = re.compile(' '.join(row.subject.strip().split("_"))+"*", re.IGNORECASE)
		sentence = sentence1.sub(r" <SUBJECT> ", sentence)
		sentence = sentence.replace(' '.join(row.subject.split("_")), " <SUBJECT> ")
		row.subject = unidecode(row.subject)
		sentence1 = re.compile(' '.join(row.subject.strip().split("_"))+"*", re.IGNORECASE)
		sentence = sentence1.sub(r" <SUBJECT> ", sentence)

		new.append(sentence)

	return new


def getSentence(row):
	"""Choose the best template from list of templates and fill in the blanks"""
	row.templates = row.templates.split('\n')
	nr_temp = {}

	for template in row.templates:
		if "<SUBJECT>" and "<OBJECT>" and "<PREDICATE>" in template:
			nr_temp[template] = 4
		elif "<SUBJECT>" and "<OBJECT>" in template:
			nr_temp[template] = 3
		elif "<SUBJECT>" and "<PREDICATE>" in template:
			nr_temp[template] = 2
		elif "<PREDICATE>" and "<OBJECT>" in template:
			nr_temp[template] = 2
		elif "<SUBJECT>" or "<OBJECT>" or "<PREDICATE>" in template:
			nr_temp[template] = 1
		else:
			nr_temp[template] = 0

	mx = max(nr_temp.values())
	best_temps = [k for k, v in nr_temp.items() if v == mx]
	final_temp = sorted(best_temps)[0]

	row.subject = re.sub(r'\([^()]*\)', '', ' '.join(row.subject.split('_')).strip())
	row.object = re.sub(r'\([^()]*\)', '', ' '.join(row.object.split('_')).strip())
	row.predicate = row.predicate.lower()

	sentence = final_temp.replace("<SUBJECT>", row.subject)
	sentence = sentence.replace("<OBJECT>", row.object)
	sentence = sentence.replace("<PREDICATE>", row.predicate)

	sentence = re.sub(r'\"', '', sentence)
	sentence = re.sub(r'  ', ' ', sentence)
	sentence = sentence.strip()

	return sentence


def calculateBLEU(row, weights):
	"""Calculate BLEU scores for the created sentences"""
	reference = [nltk.word_tokenize(ref) for ref in row.lex]
	candidate = nltk.word_tokenize(row.sentence)

	score = sentence_bleu(reference, candidate, weights)

	return score


def main():
	df_train, df_test = getData()

	# Regex to split predicate 
	re_outer = re.compile(r'([^A-Z ])([A-Z])')
	re_inner = re.compile(r'(?<!^)([A-Z])([^A-Z])')

	# Some preprocessing 
	df_test[['subject', 'predicate', 'object']] = df_test['triple'].str.split('|', expand=True)
	df_test['superclass'] = df_test['subject'].apply(lambda x: getSuperclass(x))
	df_test['predicate'] = df_test['predicate'].apply(lambda x: re_outer.sub(r'\1 \2', re_inner.sub(r'\1\2', x)))

	df_train[['subject', 'predicate', 'object']] = df_train['triple'].str.split('|', expand=True)
	df_train['superclass'] = df_train['subject'].apply(lambda x: getSuperclass(x))
	df_train['predicate'] = df_train['predicate'].apply(lambda x: re_outer.sub(r'\1 \2', re_inner.sub(r'\1\2', x)))
	df_train['lex_temp'] = df_train.apply(lambda row: replaceSubObj(row), axis=1)
	df_train['lex_temp'] = df_train['lex_temp'].apply(lambda x: '\n'.join(x))

	# Change templates to labels
	label_encoder = LabelEncoder()
	df_train['lex_label'] = label_encoder.fit_transform(df_train['lex_temp'])

	# Classification
	pipeline = Pipeline([
            ('union', ColumnTransformer([
           	('text_vec1', CountVectorizer(), 'category'),
           	('text_vec2', CountVectorizer(), 'superclass'),
           	('text_vec3', CountVectorizer(ngram_range=(1,2)), 'predicate')])),
            ('clf', KNeighborsClassifier(n_neighbors=1))])

	model = pipeline.fit(df_train[['category', 'superclass', 'predicate']], df_train.lex_label)
	prediction = model.predict(df_test[['category', 'superclass', 'predicate']])

	# Get predicated templates and final sentence
	templates = label_encoder.inverse_transform(prediction)
	df_test['templates'] =  templates
	df_test['sentence'] = df_test.apply(lambda row: getSentence(row), axis=1) 

	# Get BLEU-1 and BLEU-4 scores
	df_test['BLEU-1'] = df_test.apply(lambda row: calculateBLEU(row, weights=(1,0,0,0)), axis=1)
	df_test['BLEU-4'] = df_test.apply(lambda row: calculateBLEU(row, weights=(0.25,0.25,0.25,0.25)), axis=1)

	print(df_test[['triple', 'BLEU-1', 'BLEU-4']])

	print("\nAverage BLEU-1 score: {}".format(df_test['BLEU-1'].mean()))
	print("Average BLEU-4 score: {}".format(df_test['BLEU-4'].mean()))

	df_test.to_csv('output.csv', columns=['triple','sentence'], sep="\t")

if __name__ == '__main__':
	main()