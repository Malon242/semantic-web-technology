local site_config = {}

site_config.LUAROCKS_PREFIX=[[/data/s1531565/torch/install/]]
site_config.LUA_INCDIR=[[/data/s1531565/torch/install/include]]
site_config.LUA_LIBDIR=[[/data/s1531565/torch/install/lib]]
site_config.LUA_BINDIR=[[/data/s1531565/torch/install/bin]]
site_config.LUA_INTERPRETER = [[lua]]
site_config.LUAROCKS_SYSCONFDIR=[[/data/s1531565/torch/install/etc/luarocks]]
site_config.LUAROCKS_ROCKS_TREE=[[/data/s1531565/torch/install/]]
site_config.LUAROCKS_ROCKS_SUBDIR=[[lib/luarocks/rocks]]
site_config.LUA_DIR_SET = true
site_config.LUAROCKS_UNAME_S=[[Linux]]
site_config.LUAROCKS_UNAME_M=[[x86_64]]
site_config.LUAROCKS_DOWNLOADER=[[wget]]
site_config.LUAROCKS_MD5CHECKER=[[md5sum]]

return site_config
