# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/data/s1531565/torch/extra/threads/lib/thread-main.c" "/data/s1531565/torch/extra/threads/build/CMakeFiles/threadsmain.dir/lib/thread-main.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAS_LUAL_SETFUNCS"
  "USE_PTHREAD_THREADS=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/data/s1531565/torch/install/include"
  "/data/s1531565/torch/install/include/TH"
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
