# Install script for directory: /data/s1531565/torch/extra/nnx

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/data/s1531565/torch/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nnx/0.1-1/lib/libnnx.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nnx/0.1-1/lib/libnnx.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nnx/0.1-1/lib/libnnx.so"
         RPATH "$ORIGIN/../lib:/data/s1531565/torch/install/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nnx/0.1-1/lib" TYPE MODULE FILES "/data/s1531565/torch/extra/nnx/build/libnnx.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nnx/0.1-1/lib/libnnx.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nnx/0.1-1/lib/libnnx.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nnx/0.1-1/lib/libnnx.so"
         OLD_RPATH "/data/s1531565/torch/install/lib:::::::::::::::"
         NEW_RPATH "$ORIGIN/../lib:/data/s1531565/torch/install/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/software/software/binutils/2.25-GCCcore-4.9.3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nnx/0.1-1/lib/libnnx.so")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nnx/0.1-1/lua/nnx" TYPE FILE FILES
    "/data/s1531565/torch/extra/nnx/Balance.lua"
    "/data/s1531565/torch/extra/nnx/CTCCriterion.lua"
    "/data/s1531565/torch/extra/nnx/DataList.lua"
    "/data/s1531565/torch/extra/nnx/DataSet.lua"
    "/data/s1531565/torch/extra/nnx/DataSetLabelMe.lua"
    "/data/s1531565/torch/extra/nnx/DataSetSamplingPascal.lua"
    "/data/s1531565/torch/extra/nnx/DistMarginCriterion.lua"
    "/data/s1531565/torch/extra/nnx/DistNLLCriterion.lua"
    "/data/s1531565/torch/extra/nnx/FunctionWrapper.lua"
    "/data/s1531565/torch/extra/nnx/LA.lua"
    "/data/s1531565/torch/extra/nnx/Minus.lua"
    "/data/s1531565/torch/extra/nnx/MultiSoftMax.lua"
    "/data/s1531565/torch/extra/nnx/PixelSort.lua"
    "/data/s1531565/torch/extra/nnx/Probe.lua"
    "/data/s1531565/torch/extra/nnx/PullTable.lua"
    "/data/s1531565/torch/extra/nnx/PushTable.lua"
    "/data/s1531565/torch/extra/nnx/QDRiemaNNLinear.lua"
    "/data/s1531565/torch/extra/nnx/SaturatedLU.lua"
    "/data/s1531565/torch/extra/nnx/SoftMaxForest.lua"
    "/data/s1531565/torch/extra/nnx/SoftMaxTree.lua"
    "/data/s1531565/torch/extra/nnx/SparseCriterion.lua"
    "/data/s1531565/torch/extra/nnx/SpatialClassifier.lua"
    "/data/s1531565/torch/extra/nnx/SpatialColorTransform.lua"
    "/data/s1531565/torch/extra/nnx/SpatialDownSampling.lua"
    "/data/s1531565/torch/extra/nnx/SpatialFovea.lua"
    "/data/s1531565/torch/extra/nnx/SpatialGraph.lua"
    "/data/s1531565/torch/extra/nnx/SpatialLinear.lua"
    "/data/s1531565/torch/extra/nnx/SpatialMatching.lua"
    "/data/s1531565/torch/extra/nnx/SpatialMaxSampling.lua"
    "/data/s1531565/torch/extra/nnx/SpatialNormalization.lua"
    "/data/s1531565/torch/extra/nnx/SpatialPadding.lua"
    "/data/s1531565/torch/extra/nnx/SpatialPyramid.lua"
    "/data/s1531565/torch/extra/nnx/SpatialRadialMatching.lua"
    "/data/s1531565/torch/extra/nnx/SpatialReSampling.lua"
    "/data/s1531565/torch/extra/nnx/SpatialReSamplingEx.lua"
    "/data/s1531565/torch/extra/nnx/SpatialRecursiveFovea.lua"
    "/data/s1531565/torch/extra/nnx/SpatialSparseCriterion.lua"
    "/data/s1531565/torch/extra/nnx/SpatialUpSampling.lua"
    "/data/s1531565/torch/extra/nnx/SuperCriterion.lua"
    "/data/s1531565/torch/extra/nnx/Tic.lua"
    "/data/s1531565/torch/extra/nnx/Toc.lua"
    "/data/s1531565/torch/extra/nnx/TreeNLLCriterion.lua"
    "/data/s1531565/torch/extra/nnx/init.lua"
    "/data/s1531565/torch/extra/nnx/test/test-all.lua"
    "/data/s1531565/torch/extra/nnx/test/test-omp.lua"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/data/s1531565/torch/extra/nnx/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
