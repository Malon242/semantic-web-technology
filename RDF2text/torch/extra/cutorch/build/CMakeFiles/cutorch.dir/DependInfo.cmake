# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/data/s1531565/torch/extra/cutorch/Storage.c" "/data/s1531565/torch/extra/cutorch/build/CMakeFiles/cutorch.dir/Storage.c.o"
  "/data/s1531565/torch/extra/cutorch/Tensor.c" "/data/s1531565/torch/extra/cutorch/build/CMakeFiles/cutorch.dir/Tensor.c.o"
  "/data/s1531565/torch/extra/cutorch/build/TensorMath.c" "/data/s1531565/torch/extra/cutorch/build/CMakeFiles/cutorch.dir/TensorMath.c.o"
  "/data/s1531565/torch/extra/cutorch/TensorOperator.c" "/data/s1531565/torch/extra/cutorch/build/CMakeFiles/cutorch.dir/TensorOperator.c.o"
  "/data/s1531565/torch/extra/cutorch/init.c" "/data/s1531565/torch/extra/cutorch/build/CMakeFiles/cutorch.dir/init.c.o"
  "/data/s1531565/torch/extra/cutorch/torch/utils.c" "/data/s1531565/torch/extra/cutorch/build/CMakeFiles/cutorch.dir/torch/utils.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CUDA_HAS_FP16=1"
  "HAS_LUAL_SETFUNCS"
  "TH_GENERIC_USE_HALF=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "lib/THC"
  "/data/s1531565/torch/install/include"
  "/data/s1531565/torch/install/include/TH"
  "/software/software/CUDA/7.5.18/include"
  "../lib/THC"
  "../torch"
  "."
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
