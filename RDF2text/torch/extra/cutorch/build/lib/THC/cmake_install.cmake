# Install script for directory: /data/s1531565/torch/extra/cutorch/lib/THC

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/data/s1531565/torch/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libTHC.so.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libTHC.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "$ORIGIN/../lib:/data/s1531565/torch/install/lib")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/data/s1531565/torch/extra/cutorch/build/lib/THC/libTHC.so.0"
    "/data/s1531565/torch/extra/cutorch/build/lib/THC/libTHC.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libTHC.so.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libTHC.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "/data/s1531565/torch/install/lib:::::::::::::::"
           NEW_RPATH "$ORIGIN/../lib:/data/s1531565/torch/install/lib")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/software/software/binutils/2.25-GCCcore-4.9.3/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/THC" TYPE FILE FILES
    "/data/s1531565/torch/extra/cutorch/lib/THC/THC.h"
    "/data/s1531565/torch/extra/cutorch/build/lib/THC/THCGeneral.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCBlas.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCSleep.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCStorage.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCStorageCopy.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCStream.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCThreadLocal.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensor.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorCopy.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorRandom.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorMath.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorConv.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCApply.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCReduce.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCReduceAll.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCReduceApplyUtils.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCAsmUtils.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCAtomics.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCScanUtils.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCSortUtils.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCAllocator.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCCachingAllocator.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCCachingHostAllocator.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCDeviceUtils.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCDeviceTensor.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCDeviceTensor-inl.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCDeviceTensorUtils.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCDeviceTensorUtils-inl.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateAllTypes.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateByteType.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateCharType.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateShortType.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateIntType.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateLongType.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateHalfType.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateFloatType.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateFloatTypes.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCGenerateDoubleType.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCHalf.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCNumerics.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorSort.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorInfo.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorMathPointwise.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorTypeUtils.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorRandom.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorMathMagma.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCThrustAllocator.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorMode.cuh"
    "/data/s1531565/torch/extra/cutorch/lib/THC/THCTensorTopK.cuh"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/THC/generic" TYPE FILE FILES
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCStorage.c"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCStorage.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCStorage.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensor.c"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensor.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensor.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCStorageCopy.c"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCStorageCopy.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCStorageCopy.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorCopy.c"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorCopy.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorCopy.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMasked.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMasked.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMath.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMath.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathBlas.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathBlas.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathCompare.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathCompare.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathCompareT.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathCompareT.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathMagma.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathMagma.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathPairwise.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathPairwise.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathPointwise.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathPointwise.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathReduce.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathReduce.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathScan.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMathScan.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorScatterGather.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorScatterGather.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorIndex.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorIndex.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorSort.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorSort.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCDeviceTensorUtils.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorRandom.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorRandom.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMode.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorMode.cu"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorTopK.h"
    "/data/s1531565/torch/extra/cutorch/lib/THC/generic/THCTensorTopK.cu"
    )
endif()

