# This is the CMakeCache file.
# For build in directory: /data/s1531565/torch/extra/cutorch/build
# It was generated by CMake: /apps/sandybridge/software/CMake/3.6.1-foss-2016a/bin/cmake
# You can edit this file to change values found and used by cmake.
# If you do not want to change any of the values, simply exit the editor.
# If you do want to change a value, simply edit, save, and exit the editor.
# The syntax for the file is as follows:
# KEY:TYPE=VALUE
# KEY is the name of a variable in the cache.
# TYPE is a hint to GUIs for the type of VALUE, DO NOT EDIT TYPE!.
# VALUE is the current value for the KEY.

########################
# EXTERNAL cache entries
########################

//Path to a program.
CMAKE_AR:FILEPATH=/software/software/binutils/2.25-GCCcore-4.9.3/bin/ar

//Choose the type of build, options are: None(CMAKE_CXX_FLAGS or
// CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel.
CMAKE_BUILD_TYPE:STRING=Release

//Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON

//CXX compiler
CMAKE_CXX_COMPILER:FILEPATH=/software/software/GCCcore/4.9.3/bin/c++

//Flags used by the compiler during all build types.
CMAKE_CXX_FLAGS:STRING=

//Flags used by the compiler during debug builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-g

//Flags used by the compiler during release builds for minimum
// size.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during release builds.
CMAKE_CXX_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during release builds with debug info.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//C compiler
CMAKE_C_COMPILER:FILEPATH=/software/software/GCCcore/4.9.3/bin/cc

//Flags used by the compiler during all build types.
CMAKE_C_FLAGS:STRING=

//Flags used by the compiler during debug builds.
CMAKE_C_FLAGS_DEBUG:STRING=-g

//Flags used by the compiler during release builds for minimum
// size.
CMAKE_C_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during release builds.
CMAKE_C_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during release builds with debug info.
CMAKE_C_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//Flags used by the linker.
CMAKE_EXE_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Enable/Disable output of compile commands during generation.
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=OFF

//Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/data/s1531565/torch/install/lib/luarocks/rocks/cutorch/scm-1

//Path to a program.
CMAKE_LINKER:FILEPATH=/software/software/binutils/2.25-GCCcore-4.9.3/bin/ld

//Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/bin/gmake

//Flags used by the linker during the creation of modules.
CMAKE_MODULE_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_NM:FILEPATH=/software/software/binutils/2.25-GCCcore-4.9.3/bin/nm

//Path to a program.
CMAKE_OBJCOPY:FILEPATH=/software/software/binutils/2.25-GCCcore-4.9.3/bin/objcopy

//Path to a program.
CMAKE_OBJDUMP:FILEPATH=/software/software/binutils/2.25-GCCcore-4.9.3/bin/objdump

//No help, variable specified on the command line.
CMAKE_PREFIX_PATH:UNINITIALIZED=/data/s1531565/torch/install/bin/..

//Value Computed by CMake
CMAKE_PROJECT_NAME:STATIC=Project

//Path to a program.
CMAKE_RANLIB:FILEPATH=/software/software/binutils/2.25-GCCcore-4.9.3/bin/ranlib

//Flags used by the linker during the creation of dll's.
CMAKE_SHARED_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//If set, runtime paths are not added when installing shared libraries,
// but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO

//If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO

//Flags used by the linker during the creation of static libraries.
CMAKE_STATIC_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_STRIP:FILEPATH=/software/software/binutils/2.25-GCCcore-4.9.3/bin/strip

//If this value is on, makefiles will be generated without the
// .SILENT directive, and all commands will be echoed to the console
// during the make.  This is useful for debugging only. With Visual
// Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE

//Compile device code in 64 bit mode
CUDA_64_BIT_DEVICE_CODE:BOOL=ON

//Attach the build rule to the CUDA source file.  Enable only when
// the CUDA source file is added to at most one target.
CUDA_ATTACH_VS_BUILD_RULE_TO_CUDA_FILE:BOOL=ON

//Generate and parse .cubin files in Device mode.
CUDA_BUILD_CUBIN:BOOL=OFF

//Build in Emulation mode
CUDA_BUILD_EMULATION:BOOL=OFF

//"cudart" library
CUDA_CUDART_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libcudart.so

//"cuda" library (older versions only).
CUDA_CUDA_LIBRARY:FILEPATH=CUDA_CUDA_LIBRARY-NOTFOUND

//Directory to put all the output files.  If blank it will default
// to the CMAKE_CURRENT_BINARY_DIR
CUDA_GENERATED_OUTPUT_DIR:PATH=

//Generated file extension
CUDA_HOST_COMPILATION_CPP:BOOL=ON

//Host side compiler used by NVCC
CUDA_HOST_COMPILER:FILEPATH=/software/software/GCCcore/4.9.3/bin/cc

//Path to a program.
CUDA_NVCC_EXECUTABLE:FILEPATH=/software/software/CUDA/7.5.18/bin/nvcc

//Semi-colon delimit multiple arguments.
CUDA_NVCC_FLAGS:STRING=

//Semi-colon delimit multiple arguments.
CUDA_NVCC_FLAGS_DEBUG:STRING=

//Semi-colon delimit multiple arguments.
CUDA_NVCC_FLAGS_MINSIZEREL:STRING=

//Semi-colon delimit multiple arguments.
CUDA_NVCC_FLAGS_RELEASE:STRING=

//Semi-colon delimit multiple arguments.
CUDA_NVCC_FLAGS_RELWITHDEBINFO:STRING=

//Propage C/CXX_FLAGS and friends to the host compiler via -Xcompile
CUDA_PROPAGATE_HOST_FLAGS:BOOL=ON

//Path to a file.
CUDA_SDK_ROOT_DIR:PATH=CUDA_SDK_ROOT_DIR-NOTFOUND

//Compile CUDA objects with separable compilation enabled.  Requires
// CUDA 5.0+
CUDA_SEPARABLE_COMPILATION:BOOL=OFF

//Path to a file.
CUDA_TOOLKIT_INCLUDE:PATH=/software/software/CUDA/7.5.18/include

//Toolkit location.
CUDA_TOOLKIT_ROOT_DIR:PATH=/software/software/CUDA/7.5.18

//Use the static version of the CUDA runtime library if available
CUDA_USE_STATIC_CUDA_RUNTIME:BOOL=OFF

//Print out the commands run while compiling the CUDA source file.
//  With the Makefile generator this defaults to VERBOSE variable
// specified on the command line, but can be forced on with this
// option.
CUDA_VERBOSE_BUILD:BOOL=OFF

//Version of CUDA as computed from nvcc.
CUDA_VERSION:STRING=7.5

//"cublas" library
CUDA_cublas_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libcublas.so

//"cublas_device" library
CUDA_cublas_device_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libcublas_device.a

//"cufft" library
CUDA_cufft_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libcufft.so

//"cupti" library
CUDA_cupti_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/extras/CUPTI/lib64/libcupti.so

//"curand" library
CUDA_curand_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libcurand.so

//"cusolver" library
CUDA_cusolver_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libcusolver.so

//"cusparse" library
CUDA_cusparse_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libcusparse.so

//"nppc" library
CUDA_nppc_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libnppc.so

//"nppi" library
CUDA_nppi_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libnppi.so

//"npps" library
CUDA_npps_LIBRARY:FILEPATH=/software/software/CUDA/7.5.18/lib64/libnpps.so

//No help, variable specified on the command line.
LUALIB:UNINITIALIZED=

//No help, variable specified on the command line.
LUA_INCDIR:UNINITIALIZED=/data/s1531565/torch/install/include

//Path to a file.
MAGMA_INCLUDE_DIR:PATH=MAGMA_INCLUDE_DIR-NOTFOUND

//Path to a library.
MAGMA_LIBRARIES:FILEPATH=MAGMA_LIBRARIES-NOTFOUND

//disable asserts (WARNING: this may result in invalid memory accesses)
NDEBUG:BOOL=OFF

//Value Computed by CMake
Project_BINARY_DIR:STATIC=/data/s1531565/torch/extra/cutorch/build

//Value Computed by CMake
Project_SOURCE_DIR:STATIC=/data/s1531565/torch/extra/cutorch

//Dependencies for the target
THC_LIB_DEPENDS:STATIC=general;/software/software/CUDA/7.5.18/lib64/libcudart.so;general;/software/software/CUDA/7.5.18/lib64/libcublas.so;general;/software/software/CUDA/7.5.18/lib64/libcublas_device.a;general;TH;general;/software/software/CUDA/7.5.18/lib64/libcurand.so;general;/software/software/CUDA/7.5.18/lib64/libcusparse.so;

//The directory containing a CMake configuration file for Torch.
Torch_DIR:PATH=/data/s1531565/torch/install/share/cmake/torch

//Build libraries with executable rpaths
WITH_RPATH:BOOL=ON

//Dependencies for the target
cutorch_LIB_DEPENDS:STATIC=general;luaT;general;THC;


########################
# INTERNAL cache entries
########################

//ADVANCED property for variable: CMAKE_AR
CMAKE_AR-ADVANCED:INTERNAL=1
//This is the directory where this CMakeCache.txt was created
CMAKE_CACHEFILE_DIR:INTERNAL=/data/s1531565/torch/extra/cutorch/build
//Major version of cmake used to create the current loaded cache
CMAKE_CACHE_MAJOR_VERSION:INTERNAL=3
//Minor version of cmake used to create the current loaded cache
CMAKE_CACHE_MINOR_VERSION:INTERNAL=6
//Patch version of cmake used to create the current loaded cache
CMAKE_CACHE_PATCH_VERSION:INTERNAL=1
//ADVANCED property for variable: CMAKE_COLOR_MAKEFILE
CMAKE_COLOR_MAKEFILE-ADVANCED:INTERNAL=1
//Path to CMake executable.
CMAKE_COMMAND:INTERNAL=/apps/sandybridge/software/CMake/3.6.1-foss-2016a/bin/cmake
//Path to cpack program executable.
CMAKE_CPACK_COMMAND:INTERNAL=/apps/sandybridge/software/CMake/3.6.1-foss-2016a/bin/cpack
//Path to ctest program executable.
CMAKE_CTEST_COMMAND:INTERNAL=/apps/sandybridge/software/CMake/3.6.1-foss-2016a/bin/ctest
//ADVANCED property for variable: CMAKE_CXX_COMPILER
CMAKE_CXX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS
CMAKE_CXX_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_DEBUG
CMAKE_CXX_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_MINSIZEREL
CMAKE_CXX_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELEASE
CMAKE_CXX_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELWITHDEBINFO
CMAKE_CXX_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER
CMAKE_C_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS
CMAKE_C_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_DEBUG
CMAKE_C_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_MINSIZEREL
CMAKE_C_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELEASE
CMAKE_C_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELWITHDEBINFO
CMAKE_C_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//Path to cache edit program executable.
CMAKE_EDIT_COMMAND:INTERNAL=/apps/sandybridge/software/CMake/3.6.1-foss-2016a/bin/ccmake
//Executable file format
CMAKE_EXECUTABLE_FORMAT:INTERNAL=ELF
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS
CMAKE_EXE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_DEBUG
CMAKE_EXE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_MINSIZEREL
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELEASE
CMAKE_EXE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXPORT_COMPILE_COMMANDS
CMAKE_EXPORT_COMPILE_COMMANDS-ADVANCED:INTERNAL=1
//Name of external makefile project generator.
CMAKE_EXTRA_GENERATOR:INTERNAL=
//Name of generator.
CMAKE_GENERATOR:INTERNAL=Unix Makefiles
//Name of generator platform.
CMAKE_GENERATOR_PLATFORM:INTERNAL=
//Name of generator toolset.
CMAKE_GENERATOR_TOOLSET:INTERNAL=
//Source directory with the top level CMakeLists.txt file for this
// project
CMAKE_HOME_DIRECTORY:INTERNAL=/data/s1531565/torch/extra/cutorch
//Install .so files without execute permission.
CMAKE_INSTALL_SO_NO_EXE:INTERNAL=0
//ADVANCED property for variable: CMAKE_LINKER
CMAKE_LINKER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MAKE_PROGRAM
CMAKE_MAKE_PROGRAM-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS
CMAKE_MODULE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_DEBUG
CMAKE_MODULE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELEASE
CMAKE_MODULE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_NM
CMAKE_NM-ADVANCED:INTERNAL=1
//number of local generators
CMAKE_NUMBER_OF_MAKEFILES:INTERNAL=3
//ADVANCED property for variable: CMAKE_OBJCOPY
CMAKE_OBJCOPY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJDUMP
CMAKE_OBJDUMP-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_RANLIB
CMAKE_RANLIB-ADVANCED:INTERNAL=1
//Path to CMake installation.
CMAKE_ROOT:INTERNAL=/apps/sandybridge/software/CMake/3.6.1-foss-2016a/share/cmake-3.6
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS
CMAKE_SHARED_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_DEBUG
CMAKE_SHARED_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELEASE
CMAKE_SHARED_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_INSTALL_RPATH
CMAKE_SKIP_INSTALL_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_RPATH
CMAKE_SKIP_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS
CMAKE_STATIC_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_DEBUG
CMAKE_STATIC_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELEASE
CMAKE_STATIC_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STRIP
CMAKE_STRIP-ADVANCED:INTERNAL=1
//uname command
CMAKE_UNAME:INTERNAL=/usr/bin/uname
//ADVANCED property for variable: CMAKE_VERBOSE_MAKEFILE
CMAKE_VERBOSE_MAKEFILE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_64_BIT_DEVICE_CODE
CUDA_64_BIT_DEVICE_CODE-ADVANCED:INTERNAL=1
//List of intermediate files that are part of the cuda dependency
// scanning.
CUDA_ADDITIONAL_CLEAN_FILES:INTERNAL=/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCReduceApplyUtils.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCBlas.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCSleep.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCStorage.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCStorageCopy.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensor.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorCopy.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorMath.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorMath2.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorMathBlas.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorMathMagma.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorMathPairwise.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorMathReduce.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorMathScan.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorIndex.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorConv.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorRandom.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorScatterGather.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorTopK.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorSort.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorTypeUtils.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCSortUtils.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCTensorMode.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorSortByte.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareTByte.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathPointwiseByte.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareByte.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathReduceByte.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMaskedByte.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorSortChar.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareTChar.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathPointwiseChar.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareChar.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathReduceChar.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMaskedChar.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorSortShort.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareTShort.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathPointwiseShort.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareShort.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathReduceShort.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMaskedShort.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorSortInt.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareTInt.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathPointwiseInt.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareInt.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathReduceInt.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMaskedInt.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorSortLong.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareTLong.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathPointwiseLong.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareLong.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathReduceLong.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMaskedLong.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorSortHalf.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareTHalf.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathPointwiseHalf.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareHalf.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathReduceHalf.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMaskedHalf.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorSortFloat.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareTFloat.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathPointwiseFloat.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareFloat.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathReduceFloat.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMaskedFloat.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorSortDouble.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareTDouble.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathPointwiseDouble.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathCompareDouble.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMathReduceDouble.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir/generated/THC_generated_THCTensorMaskedDouble.cu.o.depend;/data/s1531565/torch/extra/cutorch/build/lib/THC/CMakeFiles/THC.dir//THC_generated_THCHalf.cu.o.depend
//ADVANCED property for variable: CUDA_ATTACH_VS_BUILD_RULE_TO_CUDA_FILE
CUDA_ATTACH_VS_BUILD_RULE_TO_CUDA_FILE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_BUILD_CUBIN
CUDA_BUILD_CUBIN-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_BUILD_EMULATION
CUDA_BUILD_EMULATION-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_CUDART_LIBRARY
CUDA_CUDART_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_CUDA_LIBRARY
CUDA_CUDA_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_GENERATED_OUTPUT_DIR
CUDA_GENERATED_OUTPUT_DIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_HOST_COMPILATION_CPP
CUDA_HOST_COMPILATION_CPP-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_NVCC_EXECUTABLE
CUDA_NVCC_EXECUTABLE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_NVCC_FLAGS
CUDA_NVCC_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_NVCC_FLAGS_DEBUG
CUDA_NVCC_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_NVCC_FLAGS_MINSIZEREL
CUDA_NVCC_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_NVCC_FLAGS_RELEASE
CUDA_NVCC_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_NVCC_FLAGS_RELWITHDEBINFO
CUDA_NVCC_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_PROPAGATE_HOST_FLAGS
CUDA_PROPAGATE_HOST_FLAGS-ADVANCED:INTERNAL=1
//This is the value of the last time CUDA_SDK_ROOT_DIR was set
// successfully.
CUDA_SDK_ROOT_DIR_INTERNAL:INTERNAL=CUDA_SDK_ROOT_DIR-NOTFOUND
//ADVANCED property for variable: CUDA_SEPARABLE_COMPILATION
CUDA_SEPARABLE_COMPILATION-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_TOOLKIT_INCLUDE
CUDA_TOOLKIT_INCLUDE-ADVANCED:INTERNAL=1
//This is the value of the last time CUDA_TOOLKIT_ROOT_DIR was
// set successfully.
CUDA_TOOLKIT_ROOT_DIR_INTERNAL:INTERNAL=/software/software/CUDA/7.5.18
//This is the value of the last time CUDA_TOOLKIT_TARGET_DIR was
// set successfully.
CUDA_TOOLKIT_TARGET_DIR_INTERNAL:INTERNAL=/software/software/CUDA/7.5.18
//ADVANCED property for variable: CUDA_VERBOSE_BUILD
CUDA_VERBOSE_BUILD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_VERSION
CUDA_VERSION-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_cublas_LIBRARY
CUDA_cublas_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_cublas_device_LIBRARY
CUDA_cublas_device_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_cufft_LIBRARY
CUDA_cufft_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_cupti_LIBRARY
CUDA_cupti_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_curand_LIBRARY
CUDA_curand_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_cusolver_LIBRARY
CUDA_cusolver_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_cusparse_LIBRARY
CUDA_cusparse_LIBRARY-ADVANCED:INTERNAL=1
//Location of make2cmake.cmake
CUDA_make2cmake:INTERNAL=/data/s1531565/torch/install/share/cmake/torch/FindCUDA/make2cmake.cmake
//ADVANCED property for variable: CUDA_nppc_LIBRARY
CUDA_nppc_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_nppi_LIBRARY
CUDA_nppi_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CUDA_npps_LIBRARY
CUDA_npps_LIBRARY-ADVANCED:INTERNAL=1
//Location of parse_cubin.cmake
CUDA_parse_cubin:INTERNAL=/data/s1531565/torch/install/share/cmake/torch/FindCUDA/parse_cubin.cmake
//Location of run_nvcc.cmake
CUDA_run_nvcc:INTERNAL=/data/s1531565/torch/install/share/cmake/torch/FindCUDA/run_nvcc.cmake
//Details about finding CUDA
FIND_PACKAGE_MESSAGE_DETAILS_CUDA:INTERNAL=[/software/software/CUDA/7.5.18][/software/software/CUDA/7.5.18/bin/nvcc][/software/software/CUDA/7.5.18/include][/software/software/CUDA/7.5.18/lib64/libcudart.so][v7.5(6.5)]
//Test HAS_LUAL_SETFUNCS
HAS_LUAL_SETFUNCS:INTERNAL=1

