# Install script for directory: /data/s1531565/torch/extra/nn

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/data/s1531565/torch/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nn/scm-1/lua/nn" TYPE FILE FILES
    "/data/s1531565/torch/extra/nn/Abs.lua"
    "/data/s1531565/torch/extra/nn/AbsCriterion.lua"
    "/data/s1531565/torch/extra/nn/Add.lua"
    "/data/s1531565/torch/extra/nn/AddConstant.lua"
    "/data/s1531565/torch/extra/nn/BCECriterion.lua"
    "/data/s1531565/torch/extra/nn/BatchNormalization.lua"
    "/data/s1531565/torch/extra/nn/Bilinear.lua"
    "/data/s1531565/torch/extra/nn/Bottle.lua"
    "/data/s1531565/torch/extra/nn/CAdd.lua"
    "/data/s1531565/torch/extra/nn/CAddTable.lua"
    "/data/s1531565/torch/extra/nn/CAddTensorTable.lua"
    "/data/s1531565/torch/extra/nn/CDivTable.lua"
    "/data/s1531565/torch/extra/nn/CMaxTable.lua"
    "/data/s1531565/torch/extra/nn/CMinTable.lua"
    "/data/s1531565/torch/extra/nn/CMul.lua"
    "/data/s1531565/torch/extra/nn/CMulTable.lua"
    "/data/s1531565/torch/extra/nn/CReLU.lua"
    "/data/s1531565/torch/extra/nn/CSubTable.lua"
    "/data/s1531565/torch/extra/nn/Clamp.lua"
    "/data/s1531565/torch/extra/nn/ClassNLLCriterion.lua"
    "/data/s1531565/torch/extra/nn/ClassSimplexCriterion.lua"
    "/data/s1531565/torch/extra/nn/Collapse.lua"
    "/data/s1531565/torch/extra/nn/Concat.lua"
    "/data/s1531565/torch/extra/nn/ConcatTable.lua"
    "/data/s1531565/torch/extra/nn/Constant.lua"
    "/data/s1531565/torch/extra/nn/Container.lua"
    "/data/s1531565/torch/extra/nn/Contiguous.lua"
    "/data/s1531565/torch/extra/nn/Convert.lua"
    "/data/s1531565/torch/extra/nn/Copy.lua"
    "/data/s1531565/torch/extra/nn/Cosine.lua"
    "/data/s1531565/torch/extra/nn/CosineDistance.lua"
    "/data/s1531565/torch/extra/nn/CosineEmbeddingCriterion.lua"
    "/data/s1531565/torch/extra/nn/Criterion.lua"
    "/data/s1531565/torch/extra/nn/CriterionTable.lua"
    "/data/s1531565/torch/extra/nn/CrossEntropyCriterion.lua"
    "/data/s1531565/torch/extra/nn/Decorator.lua"
    "/data/s1531565/torch/extra/nn/DepthConcat.lua"
    "/data/s1531565/torch/extra/nn/DistKLDivCriterion.lua"
    "/data/s1531565/torch/extra/nn/DistanceRatioCriterion.lua"
    "/data/s1531565/torch/extra/nn/DontCast.lua"
    "/data/s1531565/torch/extra/nn/DotProduct.lua"
    "/data/s1531565/torch/extra/nn/Dropout.lua"
    "/data/s1531565/torch/extra/nn/ELU.lua"
    "/data/s1531565/torch/extra/nn/ErrorMessages.lua"
    "/data/s1531565/torch/extra/nn/Euclidean.lua"
    "/data/s1531565/torch/extra/nn/Exp.lua"
    "/data/s1531565/torch/extra/nn/FlattenTable.lua"
    "/data/s1531565/torch/extra/nn/GPU.lua"
    "/data/s1531565/torch/extra/nn/GatedLinearUnit.lua"
    "/data/s1531565/torch/extra/nn/GradientReversal.lua"
    "/data/s1531565/torch/extra/nn/HardShrink.lua"
    "/data/s1531565/torch/extra/nn/HardTanh.lua"
    "/data/s1531565/torch/extra/nn/HingeEmbeddingCriterion.lua"
    "/data/s1531565/torch/extra/nn/Identity.lua"
    "/data/s1531565/torch/extra/nn/Index.lua"
    "/data/s1531565/torch/extra/nn/IndexLinear.lua"
    "/data/s1531565/torch/extra/nn/Jacobian.lua"
    "/data/s1531565/torch/extra/nn/JoinTable.lua"
    "/data/s1531565/torch/extra/nn/Kmeans.lua"
    "/data/s1531565/torch/extra/nn/L1Cost.lua"
    "/data/s1531565/torch/extra/nn/L1HingeEmbeddingCriterion.lua"
    "/data/s1531565/torch/extra/nn/L1Penalty.lua"
    "/data/s1531565/torch/extra/nn/LayerNormalization.lua"
    "/data/s1531565/torch/extra/nn/LeakyReLU.lua"
    "/data/s1531565/torch/extra/nn/Linear.lua"
    "/data/s1531565/torch/extra/nn/LinearWeightNorm.lua"
    "/data/s1531565/torch/extra/nn/Log.lua"
    "/data/s1531565/torch/extra/nn/LogSigmoid.lua"
    "/data/s1531565/torch/extra/nn/LogSoftMax.lua"
    "/data/s1531565/torch/extra/nn/LookupTable.lua"
    "/data/s1531565/torch/extra/nn/MM.lua"
    "/data/s1531565/torch/extra/nn/MSECriterion.lua"
    "/data/s1531565/torch/extra/nn/MV.lua"
    "/data/s1531565/torch/extra/nn/MapTable.lua"
    "/data/s1531565/torch/extra/nn/MarginCriterion.lua"
    "/data/s1531565/torch/extra/nn/MarginRankingCriterion.lua"
    "/data/s1531565/torch/extra/nn/MaskedSelect.lua"
    "/data/s1531565/torch/extra/nn/Max.lua"
    "/data/s1531565/torch/extra/nn/Maxout.lua"
    "/data/s1531565/torch/extra/nn/Mean.lua"
    "/data/s1531565/torch/extra/nn/Min.lua"
    "/data/s1531565/torch/extra/nn/MixtureTable.lua"
    "/data/s1531565/torch/extra/nn/Module.lua"
    "/data/s1531565/torch/extra/nn/ModuleCriterion.lua"
    "/data/s1531565/torch/extra/nn/Mul.lua"
    "/data/s1531565/torch/extra/nn/MulConstant.lua"
    "/data/s1531565/torch/extra/nn/MultiCriterion.lua"
    "/data/s1531565/torch/extra/nn/MultiLabelMarginCriterion.lua"
    "/data/s1531565/torch/extra/nn/MultiLabelSoftMarginCriterion.lua"
    "/data/s1531565/torch/extra/nn/MultiMarginCriterion.lua"
    "/data/s1531565/torch/extra/nn/NaN.lua"
    "/data/s1531565/torch/extra/nn/Narrow.lua"
    "/data/s1531565/torch/extra/nn/NarrowTable.lua"
    "/data/s1531565/torch/extra/nn/Normalize.lua"
    "/data/s1531565/torch/extra/nn/OneHot.lua"
    "/data/s1531565/torch/extra/nn/PReLU.lua"
    "/data/s1531565/torch/extra/nn/Padding.lua"
    "/data/s1531565/torch/extra/nn/PairwiseDistance.lua"
    "/data/s1531565/torch/extra/nn/Parallel.lua"
    "/data/s1531565/torch/extra/nn/ParallelCriterion.lua"
    "/data/s1531565/torch/extra/nn/ParallelTable.lua"
    "/data/s1531565/torch/extra/nn/PartialLinear.lua"
    "/data/s1531565/torch/extra/nn/PixelShuffle.lua"
    "/data/s1531565/torch/extra/nn/Power.lua"
    "/data/s1531565/torch/extra/nn/PrintSize.lua"
    "/data/s1531565/torch/extra/nn/Profile.lua"
    "/data/s1531565/torch/extra/nn/RReLU.lua"
    "/data/s1531565/torch/extra/nn/ReLU.lua"
    "/data/s1531565/torch/extra/nn/ReLU6.lua"
    "/data/s1531565/torch/extra/nn/Replicate.lua"
    "/data/s1531565/torch/extra/nn/Reshape.lua"
    "/data/s1531565/torch/extra/nn/Select.lua"
    "/data/s1531565/torch/extra/nn/SelectTable.lua"
    "/data/s1531565/torch/extra/nn/Sequential.lua"
    "/data/s1531565/torch/extra/nn/Sigmoid.lua"
    "/data/s1531565/torch/extra/nn/SmoothL1Criterion.lua"
    "/data/s1531565/torch/extra/nn/SoftMarginCriterion.lua"
    "/data/s1531565/torch/extra/nn/SoftMax.lua"
    "/data/s1531565/torch/extra/nn/SoftMin.lua"
    "/data/s1531565/torch/extra/nn/SoftPlus.lua"
    "/data/s1531565/torch/extra/nn/SoftShrink.lua"
    "/data/s1531565/torch/extra/nn/SoftSign.lua"
    "/data/s1531565/torch/extra/nn/SparseJacobian.lua"
    "/data/s1531565/torch/extra/nn/SparseLinear.lua"
    "/data/s1531565/torch/extra/nn/SpatialAdaptiveAveragePooling.lua"
    "/data/s1531565/torch/extra/nn/SpatialAdaptiveMaxPooling.lua"
    "/data/s1531565/torch/extra/nn/SpatialAutoCropMSECriterion.lua"
    "/data/s1531565/torch/extra/nn/SpatialAveragePooling.lua"
    "/data/s1531565/torch/extra/nn/SpatialBatchNormalization.lua"
    "/data/s1531565/torch/extra/nn/SpatialClassNLLCriterion.lua"
    "/data/s1531565/torch/extra/nn/SpatialContrastiveNormalization.lua"
    "/data/s1531565/torch/extra/nn/SpatialConvolution.lua"
    "/data/s1531565/torch/extra/nn/SpatialConvolutionLocal.lua"
    "/data/s1531565/torch/extra/nn/SpatialConvolutionMM.lua"
    "/data/s1531565/torch/extra/nn/SpatialConvolutionMap.lua"
    "/data/s1531565/torch/extra/nn/SpatialCrossMapLRN.lua"
    "/data/s1531565/torch/extra/nn/SpatialDepthWiseConvolution.lua"
    "/data/s1531565/torch/extra/nn/SpatialDilatedConvolution.lua"
    "/data/s1531565/torch/extra/nn/SpatialDilatedMaxPooling.lua"
    "/data/s1531565/torch/extra/nn/SpatialDivisiveNormalization.lua"
    "/data/s1531565/torch/extra/nn/SpatialDropout.lua"
    "/data/s1531565/torch/extra/nn/SpatialFractionalMaxPooling.lua"
    "/data/s1531565/torch/extra/nn/SpatialFullConvolution.lua"
    "/data/s1531565/torch/extra/nn/SpatialFullConvolutionMap.lua"
    "/data/s1531565/torch/extra/nn/SpatialLPPooling.lua"
    "/data/s1531565/torch/extra/nn/SpatialLogSoftMax.lua"
    "/data/s1531565/torch/extra/nn/SpatialMaxPooling.lua"
    "/data/s1531565/torch/extra/nn/SpatialMaxUnpooling.lua"
    "/data/s1531565/torch/extra/nn/SpatialReflectionPadding.lua"
    "/data/s1531565/torch/extra/nn/SpatialReplicationPadding.lua"
    "/data/s1531565/torch/extra/nn/SpatialSoftMax.lua"
    "/data/s1531565/torch/extra/nn/SpatialSubSampling.lua"
    "/data/s1531565/torch/extra/nn/SpatialSubtractiveNormalization.lua"
    "/data/s1531565/torch/extra/nn/SpatialUpSamplingBilinear.lua"
    "/data/s1531565/torch/extra/nn/SpatialUpSamplingNearest.lua"
    "/data/s1531565/torch/extra/nn/SpatialZeroPadding.lua"
    "/data/s1531565/torch/extra/nn/SplitTable.lua"
    "/data/s1531565/torch/extra/nn/Sqrt.lua"
    "/data/s1531565/torch/extra/nn/Square.lua"
    "/data/s1531565/torch/extra/nn/Squeeze.lua"
    "/data/s1531565/torch/extra/nn/StochasticGradient.lua"
    "/data/s1531565/torch/extra/nn/Sum.lua"
    "/data/s1531565/torch/extra/nn/THNN.lua"
    "/data/s1531565/torch/extra/nn/THNN_h.lua"
    "/data/s1531565/torch/extra/nn/Tanh.lua"
    "/data/s1531565/torch/extra/nn/TanhShrink.lua"
    "/data/s1531565/torch/extra/nn/TemporalConvolution.lua"
    "/data/s1531565/torch/extra/nn/TemporalDynamicKMaxPooling.lua"
    "/data/s1531565/torch/extra/nn/TemporalMaxPooling.lua"
    "/data/s1531565/torch/extra/nn/TemporalRowConvolution.lua"
    "/data/s1531565/torch/extra/nn/TemporalSubSampling.lua"
    "/data/s1531565/torch/extra/nn/Threshold.lua"
    "/data/s1531565/torch/extra/nn/Transpose.lua"
    "/data/s1531565/torch/extra/nn/Unsqueeze.lua"
    "/data/s1531565/torch/extra/nn/View.lua"
    "/data/s1531565/torch/extra/nn/VolumetricAveragePooling.lua"
    "/data/s1531565/torch/extra/nn/VolumetricBatchNormalization.lua"
    "/data/s1531565/torch/extra/nn/VolumetricConvolution.lua"
    "/data/s1531565/torch/extra/nn/VolumetricDilatedConvolution.lua"
    "/data/s1531565/torch/extra/nn/VolumetricDilatedMaxPooling.lua"
    "/data/s1531565/torch/extra/nn/VolumetricDropout.lua"
    "/data/s1531565/torch/extra/nn/VolumetricFractionalMaxPooling.lua"
    "/data/s1531565/torch/extra/nn/VolumetricFullConvolution.lua"
    "/data/s1531565/torch/extra/nn/VolumetricMaxPooling.lua"
    "/data/s1531565/torch/extra/nn/VolumetricMaxUnpooling.lua"
    "/data/s1531565/torch/extra/nn/VolumetricReplicationPadding.lua"
    "/data/s1531565/torch/extra/nn/WeightNorm.lua"
    "/data/s1531565/torch/extra/nn/WeightedEuclidean.lua"
    "/data/s1531565/torch/extra/nn/WeightedMSECriterion.lua"
    "/data/s1531565/torch/extra/nn/WhiteNoise.lua"
    "/data/s1531565/torch/extra/nn/ZeroGrad.lua"
    "/data/s1531565/torch/extra/nn/ZipTable.lua"
    "/data/s1531565/torch/extra/nn/ZipTableOneToMany.lua"
    "/data/s1531565/torch/extra/nn/hessian.lua"
    "/data/s1531565/torch/extra/nn/init.lua"
    "/data/s1531565/torch/extra/nn/test.lua"
    "/data/s1531565/torch/extra/nn/utils.lua"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nn/scm-1/lua/nn" TYPE DIRECTORY FILES "/data/s1531565/torch/extra/nn/doc")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/nn/scm-1/lua/nn" TYPE FILE FILES "/data/s1531565/torch/extra/nn/README.md")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/data/s1531565/torch/extra/nn/build/lib/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/data/s1531565/torch/extra/nn/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
