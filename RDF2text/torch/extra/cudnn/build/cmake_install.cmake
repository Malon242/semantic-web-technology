# Install script for directory: /data/s1531565/torch/extra/cudnn

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/data/s1531565/torch/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/cudnn/scm-1/lua/cudnn" TYPE FILE FILES
    "/data/s1531565/torch/extra/cudnn/BGRU.lua"
    "/data/s1531565/torch/extra/cudnn/BLSTM.lua"
    "/data/s1531565/torch/extra/cudnn/BatchNormalization.lua"
    "/data/s1531565/torch/extra/cudnn/ClippedReLU.lua"
    "/data/s1531565/torch/extra/cudnn/GRU.lua"
    "/data/s1531565/torch/extra/cudnn/LSTM.lua"
    "/data/s1531565/torch/extra/cudnn/LogSoftMax.lua"
    "/data/s1531565/torch/extra/cudnn/Pointwise.lua"
    "/data/s1531565/torch/extra/cudnn/Pooling.lua"
    "/data/s1531565/torch/extra/cudnn/Pooling3D.lua"
    "/data/s1531565/torch/extra/cudnn/RNN.lua"
    "/data/s1531565/torch/extra/cudnn/RNNReLU.lua"
    "/data/s1531565/torch/extra/cudnn/RNNTanh.lua"
    "/data/s1531565/torch/extra/cudnn/ReLU.lua"
    "/data/s1531565/torch/extra/cudnn/Sigmoid.lua"
    "/data/s1531565/torch/extra/cudnn/SoftMax.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialAveragePooling.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialBatchNormalization.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialConvolution.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialCrossEntropyCriterion.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialCrossMapLRN.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialDivisiveNormalization.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialFullConvolution.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialLogSoftMax.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialMaxPooling.lua"
    "/data/s1531565/torch/extra/cudnn/SpatialSoftMax.lua"
    "/data/s1531565/torch/extra/cudnn/Tanh.lua"
    "/data/s1531565/torch/extra/cudnn/TemporalConvolution.lua"
    "/data/s1531565/torch/extra/cudnn/VolumetricAveragePooling.lua"
    "/data/s1531565/torch/extra/cudnn/VolumetricBatchNormalization.lua"
    "/data/s1531565/torch/extra/cudnn/VolumetricConvolution.lua"
    "/data/s1531565/torch/extra/cudnn/VolumetricCrossEntropyCriterion.lua"
    "/data/s1531565/torch/extra/cudnn/VolumetricFullConvolution.lua"
    "/data/s1531565/torch/extra/cudnn/VolumetricLogSoftMax.lua"
    "/data/s1531565/torch/extra/cudnn/VolumetricMaxPooling.lua"
    "/data/s1531565/torch/extra/cudnn/VolumetricSoftMax.lua"
    "/data/s1531565/torch/extra/cudnn/convert.lua"
    "/data/s1531565/torch/extra/cudnn/env.lua"
    "/data/s1531565/torch/extra/cudnn/ffi.lua"
    "/data/s1531565/torch/extra/cudnn/find.lua"
    "/data/s1531565/torch/extra/cudnn/functional.lua"
    "/data/s1531565/torch/extra/cudnn/init.lua"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/data/s1531565/torch/extra/cudnn/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
