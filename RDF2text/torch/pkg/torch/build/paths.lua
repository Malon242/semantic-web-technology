local paths = {}

paths.install_prefix = [[/data/s1531565/torch/install]]
paths.install_bin = [[/data/s1531565/torch/install/bin]]
paths.install_man = [[/data/s1531565/torch/install/share/man]]
paths.install_lib = [[/data/s1531565/torch/install/lib]]
paths.install_share = [[/data/s1531565/torch/install/share]]
paths.install_include = [[/data/s1531565/torch/install/include]]
paths.install_cmake = [[/data/s1531565/torch/install/share/cmake/torch]]

return paths
