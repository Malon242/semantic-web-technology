# Install script for directory: /data/s1531565/torch/pkg/torch/lib/TH

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/data/s1531565/torch/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libTH.so.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libTH.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "$ORIGIN/../lib")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/data/s1531565/torch/pkg/torch/build/lib/TH/libTH.so.0"
    "/data/s1531565/torch/pkg/torch/build/lib/TH/libTH.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libTH.so.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libTH.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "::::::::::::::"
           NEW_RPATH "$ORIGIN/../lib")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/software/software/binutils/2.25-GCCcore-4.9.3/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/TH" TYPE FILE FILES
    "/data/s1531565/torch/pkg/torch/lib/TH/TH.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THAllocator.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THMath.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THBlas.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THDiskFile.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THFile.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THFilePrivate.h"
    "/data/s1531565/torch/pkg/torch/build/lib/TH/THGeneral.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateAllTypes.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateDoubleType.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateFloatType.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateHalfType.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateLongType.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateIntType.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateShortType.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateCharType.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateByteType.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateFloatTypes.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THGenerateIntTypes.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THLapack.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THLogAdd.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THMemoryFile.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THRandom.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THSize.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THStorage.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THTensor.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THTensorApply.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THTensorDimApply.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THTensorMacros.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THVector.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THAtomic.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/THHalf.h"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/TH/vector" TYPE FILE FILES
    "/data/s1531565/torch/pkg/torch/lib/TH/vector/AVX.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/vector/AVX2.h"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/TH/generic" TYPE FILE FILES
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THBlas.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THBlas.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THLapack.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THLapack.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THStorage.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THStorage.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THStorageCopy.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THStorageCopy.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensor.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensor.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorConv.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorConv.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorCopy.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorCopy.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorLapack.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorLapack.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorMath.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorMath.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorRandom.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THTensorRandom.h"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THVectorDispatch.c"
    "/data/s1531565/torch/pkg/torch/lib/TH/generic/THVector.h"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake/torch" TYPE FILE FILES "/data/s1531565/torch/pkg/torch/build/lib/TH/cmake-exports/THConfig.cmake")
endif()

