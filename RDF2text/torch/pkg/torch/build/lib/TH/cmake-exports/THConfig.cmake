# Find the TH includes and library
#
# TH_INCLUDE_DIR -- where to find the includes
# TH_LIBRARIES -- list of libraries to link against
# TH_FOUND -- set to 1 if found

SET(TH_FOUND 1)
SET(TH_INCLUDE_DIR "/data/s1531565/torch/install/include/TH")
SET(TH_LIBRARIES "/data/s1531565/torch/install/lib/libTH.so")
