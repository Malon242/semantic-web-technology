# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/data/s1531565/torch/pkg/torch/DiskFile.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/DiskFile.c.o"
  "/data/s1531565/torch/pkg/torch/File.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/File.c.o"
  "/data/s1531565/torch/pkg/torch/Generator.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/Generator.c.o"
  "/data/s1531565/torch/pkg/torch/MemoryFile.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/MemoryFile.c.o"
  "/data/s1531565/torch/pkg/torch/PipeFile.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/PipeFile.c.o"
  "/data/s1531565/torch/pkg/torch/Storage.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/Storage.c.o"
  "/data/s1531565/torch/pkg/torch/Tensor.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/Tensor.c.o"
  "/data/s1531565/torch/pkg/torch/build/TensorMath.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/TensorMath.c.o"
  "/data/s1531565/torch/pkg/torch/TensorOperator.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/TensorOperator.c.o"
  "/data/s1531565/torch/pkg/torch/Timer.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/Timer.c.o"
  "/data/s1531565/torch/pkg/torch/init.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/init.c.o"
  "/data/s1531565/torch/pkg/torch/build/random.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/random.c.o"
  "/data/s1531565/torch/pkg/torch/utils.c" "/data/s1531565/torch/pkg/torch/build/CMakeFiles/torch.dir/utils.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../lib/luaT"
  "lib/TH"
  "../lib/TH"
  "/data/s1531565/torch/install/include"
  "."
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/data/s1531565/torch/pkg/torch/build/lib/luaT/CMakeFiles/luaT.dir/DependInfo.cmake"
  "/data/s1531565/torch/pkg/torch/build/lib/TH/CMakeFiles/TH.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
