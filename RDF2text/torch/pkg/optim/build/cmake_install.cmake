# Install script for directory: /data/s1531565/torch/pkg/optim

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/data/s1531565/torch/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/optim/1.0.5-0/lua/optim" TYPE FILE FILES
    "/data/s1531565/torch/pkg/optim/ConfusionMatrix.lua"
    "/data/s1531565/torch/pkg/optim/Logger.lua"
    "/data/s1531565/torch/pkg/optim/adadelta.lua"
    "/data/s1531565/torch/pkg/optim/adagrad.lua"
    "/data/s1531565/torch/pkg/optim/adam.lua"
    "/data/s1531565/torch/pkg/optim/adamax.lua"
    "/data/s1531565/torch/pkg/optim/asgd.lua"
    "/data/s1531565/torch/pkg/optim/cg.lua"
    "/data/s1531565/torch/pkg/optim/checkgrad.lua"
    "/data/s1531565/torch/pkg/optim/cmaes.lua"
    "/data/s1531565/torch/pkg/optim/de.lua"
    "/data/s1531565/torch/pkg/optim/fista.lua"
    "/data/s1531565/torch/pkg/optim/init.lua"
    "/data/s1531565/torch/pkg/optim/lbfgs.lua"
    "/data/s1531565/torch/pkg/optim/lswolfe.lua"
    "/data/s1531565/torch/pkg/optim/nag.lua"
    "/data/s1531565/torch/pkg/optim/polyinterp.lua"
    "/data/s1531565/torch/pkg/optim/rmsprop.lua"
    "/data/s1531565/torch/pkg/optim/rprop.lua"
    "/data/s1531565/torch/pkg/optim/sgd.lua"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/optim/1.0.5-0/lua/optim" TYPE DIRECTORY FILES "/data/s1531565/torch/pkg/optim/doc")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/luarocks/rocks/optim/1.0.5-0/lua/optim" TYPE FILE FILES "/data/s1531565/torch/pkg/optim/README.md")
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/data/s1531565/torch/pkg/optim/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
