/********************************************************************************
** Form generated from reading UI file 'qluagotodialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QLUAGOTODIALOG_H
#define UI_QLUAGOTODIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_QLuaGotoDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpinBox *spinBox;
    QSpacerItem *horizontalSpacer;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *QLuaGotoDialog)
    {
        if (QLuaGotoDialog->objectName().isEmpty())
            QLuaGotoDialog->setObjectName(QString::fromUtf8("QLuaGotoDialog"));
        QLuaGotoDialog->resize(238, 92);
        verticalLayout = new QVBoxLayout(QLuaGotoDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(QLuaGotoDialog);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        spinBox = new QSpinBox(QLuaGotoDialog);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setMinimum(1);
        spinBox->setSingleStep(1);

        horizontalLayout->addWidget(spinBox);


        verticalLayout->addLayout(horizontalLayout);

        horizontalSpacer = new QSpacerItem(220, 0, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        verticalLayout->addItem(horizontalSpacer);

        buttonBox = new QDialogButtonBox(QLuaGotoDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);

#ifndef QT_NO_SHORTCUT
        label->setBuddy(spinBox);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(spinBox, buttonBox);

        retranslateUi(QLuaGotoDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), QLuaGotoDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), QLuaGotoDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(QLuaGotoDialog);
    } // setupUi

    void retranslateUi(QDialog *QLuaGotoDialog)
    {
        QLuaGotoDialog->setWindowTitle(QApplication::translate("QLuaGotoDialog", "Go to Line", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("QLuaGotoDialog", "Go to line:", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class QLuaGotoDialog: public Ui_QLuaGotoDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QLUAGOTODIALOG_H
