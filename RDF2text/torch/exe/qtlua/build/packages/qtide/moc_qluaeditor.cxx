/****************************************************************************
** Meta object code from reading C++ file 'qluaeditor.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtide/qluaeditor.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluaeditor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      36,   14, // methods
       1,  194, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      34,   17,   12,   11, 0x0a,
      66,   57,   12,   11, 0x2a,
      84,   11,   12,   11, 0x0a,
      98,   11,   11,   11, 0x0a,
     116,   11,   11,   11, 0x0a,
     136,   11,   11,   11, 0x0a,
     152,   11,   11,   11, 0x0a,
     161,   11,   11,   11, 0x0a,
     172,   11,   11,   11, 0x0a,
     182,   11,   11,   11, 0x0a,
     196,   11,   11,   11, 0x0a,
     205,   11,   11,   11, 0x0a,
     214,   11,   11,   11, 0x0a,
     222,   11,   11,   11, 0x0a,
     231,   11,   11,   11, 0x0a,
     241,   11,   11,   11, 0x0a,
     250,   11,   11,   11, 0x0a,
     259,   11,   11,   11, 0x0a,
     271,   11,   11,   11, 0x0a,
     304,   11,   11,   11, 0x0a,
     321,   11,   11,   11, 0x0a,
     341,   11,   11,   11, 0x0a,
     359,   11,   11,   11, 0x0a,
     378,   11,   11,   11, 0x0a,
     396,   11,   11,   11, 0x0a,
     415,   11,   11,   11, 0x0a,
     427,   11,   11,   11, 0x0a,
     436,   11,   11,   11, 0x0a,
     445,   11,   11,   11, 0x0a,

 // methods: signature, parameters, type, tag, flags
     462,  457,   12,   11, 0x02,
     491,  479,   12,   11, 0x02,
     514,  457,   12,   11, 0x22,
     538,  532,   12,   11, 0x02,
     569,  556,   12,   11, 0x02,
     593,  532,   12,   11, 0x22,
     626,   11,  612,   11, 0x02,

 // properties: name, type, flags
      57,  635, 0x0a095103,

       0        // eod
};

static const char qt_meta_stringdata_QLuaEditor[] = {
    "QLuaEditor\0\0bool\0fileName,inOther\0"
    "openFile(QString,bool)\0fileName\0"
    "openFile(QString)\0newDocument()\0"
    "updateStatusBar()\0updateWindowTitle()\0"
    "updateActions()\0doSave()\0doSaveAs()\0"
    "doPrint()\0doSelectAll()\0doUndo()\0"
    "doRedo()\0doCut()\0doCopy()\0doPaste()\0"
    "doGoto()\0doFind()\0doReplace()\0"
    "doMode(QLuaTextEditModeFactory*)\0"
    "doLineWrap(bool)\0doLineNumbers(bool)\0"
    "doHighlight(bool)\0doAutoIndent(bool)\0"
    "doAutoMatch(bool)\0doCompletion(bool)\0"
    "doBalance()\0doLoad()\0doEval()\0doRestart()\0"
    "file\0readFile(QFile&)\0file,rename\0"
    "writeFile(QFile&,bool)\0writeFile(QFile&)\0"
    "fname\0readFile(QString)\0fname,rename\0"
    "writeFile(QString,bool)\0writeFile(QString)\0"
    "QLuaTextEdit*\0widget()\0QString\0"
};

void QLuaEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaEditor *_t = static_cast<QLuaEditor *>(_o);
        switch (_id) {
        case 0: { bool _r = _t->openFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 1: { bool _r = _t->openFile((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: { bool _r = _t->newDocument();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 3: _t->updateStatusBar(); break;
        case 4: _t->updateWindowTitle(); break;
        case 5: _t->updateActions(); break;
        case 6: _t->doSave(); break;
        case 7: _t->doSaveAs(); break;
        case 8: _t->doPrint(); break;
        case 9: _t->doSelectAll(); break;
        case 10: _t->doUndo(); break;
        case 11: _t->doRedo(); break;
        case 12: _t->doCut(); break;
        case 13: _t->doCopy(); break;
        case 14: _t->doPaste(); break;
        case 15: _t->doGoto(); break;
        case 16: _t->doFind(); break;
        case 17: _t->doReplace(); break;
        case 18: _t->doMode((*reinterpret_cast< QLuaTextEditModeFactory*(*)>(_a[1]))); break;
        case 19: _t->doLineWrap((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->doLineNumbers((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->doHighlight((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: _t->doAutoIndent((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->doAutoMatch((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->doCompletion((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->doBalance(); break;
        case 26: _t->doLoad(); break;
        case 27: _t->doEval(); break;
        case 28: _t->doRestart(); break;
        case 29: { bool _r = _t->readFile((*reinterpret_cast< QFile(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 30: { bool _r = _t->writeFile((*reinterpret_cast< QFile(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 31: { bool _r = _t->writeFile((*reinterpret_cast< QFile(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 32: { bool _r = _t->readFile((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 33: { bool _r = _t->writeFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 34: { bool _r = _t->writeFile((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 35: { QLuaTextEdit* _r = _t->widget();
            if (_a[0]) *reinterpret_cast< QLuaTextEdit**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaEditor::staticMetaObject = {
    { &QLuaMainWindow::staticMetaObject, qt_meta_stringdata_QLuaEditor,
      qt_meta_data_QLuaEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaEditor))
        return static_cast<void*>(const_cast< QLuaEditor*>(this));
    return QLuaMainWindow::qt_metacast(_clname);
}

int QLuaEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLuaMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 36)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 36;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = fileName(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setFileName(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
