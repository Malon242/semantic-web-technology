/********************************************************************************
** Form generated from reading UI file 'qluafinddialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QLUAFINDDIALOG_H
#define UI_QLUAFINDDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_QLuaFindDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *findLabel;
    QLineEdit *findEdit;
    QHBoxLayout *hboxLayout1;
    QCheckBox *searchBackwardsBox;
    QSpacerItem *spacerItem;
    QCheckBox *caseSensitiveBox;
    QSpacerItem *spacerItem1;
    QCheckBox *wholeWordsBox;
    QHBoxLayout *hboxLayout2;
    QSpacerItem *spacerItem2;
    QPushButton *findButton;
    QPushButton *closeButton;

    void setupUi(QDialog *QLuaFindDialog)
    {
        if (QLuaFindDialog->objectName().isEmpty())
            QLuaFindDialog->setObjectName(QString::fromUtf8("QLuaFindDialog"));
        QLuaFindDialog->resize(454, 117);
        vboxLayout = new QVBoxLayout(QLuaFindDialog);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        findLabel = new QLabel(QLuaFindDialog);
        findLabel->setObjectName(QString::fromUtf8("findLabel"));

        hboxLayout->addWidget(findLabel);

        findEdit = new QLineEdit(QLuaFindDialog);
        findEdit->setObjectName(QString::fromUtf8("findEdit"));

        hboxLayout->addWidget(findEdit);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        searchBackwardsBox = new QCheckBox(QLuaFindDialog);
        searchBackwardsBox->setObjectName(QString::fromUtf8("searchBackwardsBox"));

        hboxLayout1->addWidget(searchBackwardsBox);

        spacerItem = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem);

        caseSensitiveBox = new QCheckBox(QLuaFindDialog);
        caseSensitiveBox->setObjectName(QString::fromUtf8("caseSensitiveBox"));

        hboxLayout1->addWidget(caseSensitiveBox);

        spacerItem1 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem1);

        wholeWordsBox = new QCheckBox(QLuaFindDialog);
        wholeWordsBox->setObjectName(QString::fromUtf8("wholeWordsBox"));

        hboxLayout1->addWidget(wholeWordsBox);


        vboxLayout->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        spacerItem2 = new QSpacerItem(20, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacerItem2);

        findButton = new QPushButton(QLuaFindDialog);
        findButton->setObjectName(QString::fromUtf8("findButton"));
        findButton->setDefault(true);

        hboxLayout2->addWidget(findButton);

        closeButton = new QPushButton(QLuaFindDialog);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));
        closeButton->setAutoDefault(false);

        hboxLayout2->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout2);

#ifndef QT_NO_SHORTCUT
        findLabel->setBuddy(findEdit);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(findEdit, searchBackwardsBox);
        QWidget::setTabOrder(searchBackwardsBox, caseSensitiveBox);
        QWidget::setTabOrder(caseSensitiveBox, wholeWordsBox);
        QWidget::setTabOrder(wholeWordsBox, findButton);
        QWidget::setTabOrder(findButton, closeButton);

        retranslateUi(QLuaFindDialog);
        QObject::connect(closeButton, SIGNAL(clicked()), QLuaFindDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(QLuaFindDialog);
    } // setupUi

    void retranslateUi(QDialog *QLuaFindDialog)
    {
        QLuaFindDialog->setWindowTitle(QApplication::translate("QLuaFindDialog", "Find", 0, QApplication::UnicodeUTF8));
        findLabel->setText(QApplication::translate("QLuaFindDialog", "&Find:", 0, QApplication::UnicodeUTF8));
        searchBackwardsBox->setText(QApplication::translate("QLuaFindDialog", "Search &backwards", 0, QApplication::UnicodeUTF8));
        caseSensitiveBox->setText(QApplication::translate("QLuaFindDialog", "&Case sensitive", 0, QApplication::UnicodeUTF8));
        wholeWordsBox->setText(QApplication::translate("QLuaFindDialog", "&Whole words", 0, QApplication::UnicodeUTF8));
        findButton->setText(QApplication::translate("QLuaFindDialog", " &Next", 0, QApplication::UnicodeUTF8));
        closeButton->setText(QApplication::translate("QLuaFindDialog", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class QLuaFindDialog: public Ui_QLuaFindDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QLUAFINDDIALOG_H
