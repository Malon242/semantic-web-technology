/****************************************************************************
** Meta object code from reading C++ file 'qluamainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtide/qluamainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluamainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaMainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x0a,
      32,   15,   15,   15, 0x0a,
      53,   15,   15,   15, 0x0a,
      90,   74,   15,   15, 0x0a,
     129,  121,   15,   15, 0x2a,
     178,  161,  156,   15, 0x0a,
     210,  201,  156,   15, 0x2a,
     228,   15,  156,   15, 0x0a,

 // methods: signature, parameters, type, tag, flags
     256,  251,  242,   15, 0x02,
     278,  251,  242,   15, 0x02,

       0        // eod
};

static const char qt_meta_stringdata_QLuaMainWindow[] = {
    "QLuaMainWindow\0\0updateActions()\0"
    "updateActionsLater()\0clearStatusMessage()\0"
    "message,timeout\0showStatusMessage(QString,int)\0"
    "message\0showStatusMessage(QString)\0"
    "bool\0fileName,inOther\0openFile(QString,bool)\0"
    "fileName\0openFile(QString)\0newDocument()\0"
    "QAction*\0what\0hasAction(QByteArray)\0"
    "stdAction(QByteArray)\0"
};

void QLuaMainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaMainWindow *_t = static_cast<QLuaMainWindow *>(_o);
        switch (_id) {
        case 0: _t->updateActions(); break;
        case 1: _t->updateActionsLater(); break;
        case 2: _t->clearStatusMessage(); break;
        case 3: _t->showStatusMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->showStatusMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: { bool _r = _t->openFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->openFile((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->newDocument();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: { QAction* _r = _t->hasAction((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QAction**>(_a[0]) = _r; }  break;
        case 9: { QAction* _r = _t->stdAction((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QAction**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaMainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaMainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_QLuaMainWindow,
      qt_meta_data_QLuaMainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaMainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaMainWindow))
        return static_cast<void*>(const_cast< QLuaMainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int QLuaMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
