/****************************************************************************
** Meta object code from reading C++ file 'qluasdimain.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtide/qluasdimain.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluasdimain.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaConsoleWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       1,   39, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
      42,   18,   18,   18, 0x0a,
      76,   64,   18,   18, 0x0a,
     108,  103,   18,   18, 0x2a,
     127,   18,   18,   18, 0x0a,

 // properties: name, type, flags
     144,  139, 0x01095103,

       0        // eod
};

static const char qt_meta_stringdata_QLuaConsoleWidget[] = {
    "QLuaConsoleWidget\0\0statusMessage(QString)\0"
    "setPrintTimings(bool)\0text,format\0"
    "addOutput(QString,QString)\0text\0"
    "addOutput(QString)\0moveToEnd()\0bool\0"
    "printTimings\0"
};

void QLuaConsoleWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaConsoleWidget *_t = static_cast<QLuaConsoleWidget *>(_o);
        switch (_id) {
        case 0: _t->statusMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->setPrintTimings((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->addOutput((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->addOutput((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->moveToEnd(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaConsoleWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaConsoleWidget::staticMetaObject = {
    { &QLuaTextEdit::staticMetaObject, qt_meta_stringdata_QLuaConsoleWidget,
      qt_meta_data_QLuaConsoleWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaConsoleWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaConsoleWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaConsoleWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaConsoleWidget))
        return static_cast<void*>(const_cast< QLuaConsoleWidget*>(this));
    return QLuaTextEdit::qt_metacast(_clname);
}

int QLuaConsoleWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLuaTextEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = printTimings(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setPrintTimings(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QLuaConsoleWidget::statusMessage(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_QLuaSdiMain[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       2,  124, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,
      24,   12,   12,   12, 0x0a,
      34,   12,   12,   12, 0x0a,
      48,   12,   12,   12, 0x0a,
      57,   12,   12,   12, 0x0a,
      66,   12,   12,   12, 0x0a,
      74,   12,   12,   12, 0x0a,
      83,   12,   12,   12, 0x0a,
      93,   12,   12,   12, 0x0a,
     102,   12,   12,   12, 0x0a,
     119,   12,   12,   12, 0x0a,
     137,   12,   12,   12, 0x0a,
     156,   12,   12,   12, 0x0a,
     174,   12,   12,   12, 0x0a,
     193,   12,   12,   12, 0x0a,
     205,   12,   12,   12, 0x0a,
     215,   12,   12,   12, 0x0a,
     224,   12,   12,   12, 0x0a,
     242,   12,   12,   12, 0x0a,
     263,   12,  258,   12, 0x0a,

 // methods: signature, parameters, type, tag, flags
     296,   12,  277,   12, 0x02,
     326,   12,  312,   12, 0x02,

 // properties: name, type, flags
     345,  341, 0x02095103,
     357,  341, 0x02095103,

       0        // eod
};

static const char qt_meta_stringdata_QLuaSdiMain[] = {
    "QLuaSdiMain\0\0doSaveAs()\0doPrint()\0"
    "doSelectAll()\0doUndo()\0doRedo()\0doCut()\0"
    "doCopy()\0doPaste()\0doFind()\0"
    "doLineWrap(bool)\0doHighlight(bool)\0"
    "doAutoIndent(bool)\0doAutoMatch(bool)\0"
    "doCompletion(bool)\0doBalance()\0doClear()\0"
    "doEval()\0updateStatusBar()\0updateActions()\0"
    "bool\0newDocument()\0QLuaConsoleWidget*\0"
    "consoleWidget()\0QLuaTextEdit*\0"
    "editorWidget()\0int\0historySize\0"
    "consoleLines\0"
};

void QLuaSdiMain::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaSdiMain *_t = static_cast<QLuaSdiMain *>(_o);
        switch (_id) {
        case 0: _t->doSaveAs(); break;
        case 1: _t->doPrint(); break;
        case 2: _t->doSelectAll(); break;
        case 3: _t->doUndo(); break;
        case 4: _t->doRedo(); break;
        case 5: _t->doCut(); break;
        case 6: _t->doCopy(); break;
        case 7: _t->doPaste(); break;
        case 8: _t->doFind(); break;
        case 9: _t->doLineWrap((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->doHighlight((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->doAutoIndent((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->doAutoMatch((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->doCompletion((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->doBalance(); break;
        case 15: _t->doClear(); break;
        case 16: _t->doEval(); break;
        case 17: _t->updateStatusBar(); break;
        case 18: _t->updateActions(); break;
        case 19: { bool _r = _t->newDocument();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 20: { QLuaConsoleWidget* _r = _t->consoleWidget();
            if (_a[0]) *reinterpret_cast< QLuaConsoleWidget**>(_a[0]) = _r; }  break;
        case 21: { QLuaTextEdit* _r = _t->editorWidget();
            if (_a[0]) *reinterpret_cast< QLuaTextEdit**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaSdiMain::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaSdiMain::staticMetaObject = {
    { &QLuaMainWindow::staticMetaObject, qt_meta_stringdata_QLuaSdiMain,
      qt_meta_data_QLuaSdiMain, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaSdiMain::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaSdiMain::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaSdiMain::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaSdiMain))
        return static_cast<void*>(const_cast< QLuaSdiMain*>(this));
    return QLuaMainWindow::qt_metacast(_clname);
}

int QLuaSdiMain::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLuaMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = historySize(); break;
        case 1: *reinterpret_cast< int*>(_v) = consoleLines(); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setHistorySize(*reinterpret_cast< int*>(_v)); break;
        case 1: setConsoleLines(*reinterpret_cast< int*>(_v)); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
