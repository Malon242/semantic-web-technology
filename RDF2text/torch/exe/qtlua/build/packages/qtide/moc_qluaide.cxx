/****************************************************************************
** Meta object code from reading C++ file 'qluaide.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtide/qluaide.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluaide.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaIde[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      58,   14, // methods
       2,  304, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x05,
      33,   26,    8,    8, 0x05,
      58,   26,    8,    8, 0x05,

 // slots: signature, parameters, type, tag, flags
     100,   94,   82,    8, 0x0a,
     116,    8,   82,    8, 0x2a,
     142,  138,  125,    8, 0x0a,
     156,    8,  125,    8, 0x2a,
     166,  138,  125,    8, 0x0a,
     198,    8,  183,    8, 0x0a,
     223,    8,  210,    8, 0x0a,
     252,    8,  239,    8, 0x0a,
     270,  268,    8,    8, 0x0a,
     291,   94,    8,    8, 0x0a,
     314,    8,    8,    8, 0x0a,
     333,    8,    8,    8, 0x0a,
     351,    8,    8,    8, 0x0a,
     371,  369,    8,    8, 0x0a,
     405,  396,    8,    8, 0x0a,
     431,    8,    8,    8, 0x2a,
     449,  369,    8,    8, 0x0a,
     478,  369,    8,    8, 0x0a,
     507,    8,    8,    8, 0x0a,
     552,  528,  523,    8, 0x0a,
     601,  584,  523,    8, 0x2a,
     633,  624,  523,    8, 0x2a,
     651,   26,  523,    8, 0x0a,
     673,    8,  523,    8, 0x2a,
     691,  687,  523,    8, 0x0a,
     714,  687,  523,    8, 0x0a,
     739,  737,  523,    8, 0x0a,
     779,  758,  754,    8, 0x0a,
     898,  882,  754,    8, 0x2a,
     995,  983,  754,    8, 0x2a,
    1063,  758, 1052,    8, 0x0a,
    1124,  882, 1052,    8, 0x2a,
    1174,  983, 1052,    8, 0x2a,
    1217, 1213, 1052,    8, 0x2a,
    1245,    8,    8,    8, 0x0a,
    1253,    8,    8,    8, 0x0a,
    1262,    8,  523,    8, 0x0a,
    1272,    8,  523,    8, 0x0a,
    1281,    8,    8,    8, 0x0a,
    1301,    8,    8,    8, 0x0a,
    1322,    8,    8,    8, 0x0a,
    1334,    8,    8,    8, 0x0a,
    1347,    8,    8,    8, 0x0a,
    1363,    8,    8,    8, 0x0a,

 // methods: signature, parameters, type, tag, flags
    1384,    8, 1372,    8, 0x02,
    1406,    8, 1394,    8, 0x02,
    1420,    8, 1394,    8, 0x02,
    1443,    8, 1434,    8, 0x02,
    1460,    8, 1434,    8, 0x02,
    1475,    8,  210,    8, 0x02,
    1485,    8,  239,    8, 0x02,
    1505,    8, 1495,    8, 0x02,
    1526, 1521, 1495,    8, 0x02,
    1566, 1561, 1552,    8, 0x02,
    1588, 1561, 1552,    8, 0x02,

 // properties: name, type, flags
    1610,  523, 0x01095103,
    1622,  523, 0x01095001,

       0        // eod
};

static const char qt_meta_stringdata_QLuaIde[] = {
    "QLuaIde\0\0windowsChanged()\0window\0"
    "prefsRequested(QWidget*)\0"
    "helpRequested(QWidget*)\0QLuaEditor*\0"
    "fname\0editor(QString)\0editor()\0"
    "QLuaBrowser*\0url\0browser(QUrl)\0browser()\0"
    "browser(QString)\0QLuaInspector*\0"
    "inspector()\0QLuaSdiMain*\0createSdiMain()\0"
    "QLuaMdiMain*\0createMdiMain()\0b\0"
    "setEditOnError(bool)\0addRecentFile(QString)\0"
    "clearRecentFiles()\0loadRecentFiles()\0"
    "saveRecentFiles()\0w\0activateWidget(QWidget*)\0"
    "returnTo\0activateConsole(QWidget*)\0"
    "activateConsole()\0loadWindowGeometry(QWidget*)\0"
    "saveWindowGeometry(QWidget*)\0"
    "updateActions()\0bool\0fileName,inOther,window\0"
    "openFile(QString,bool,QWidget*)\0"
    "fileName,inOther\0openFile(QString,bool)\0"
    "fileName\0openFile(QString)\0"
    "newDocument(QWidget*)\0newDocument()\0"
    "cmd\0luaExecute(QByteArray)\0"
    "luaRestart(QByteArray)\0r\0quit(QWidget*)\0"
    "int\0t,m,buttons,def,icon\0"
    "messageBox(QString,QString,QMessageBox::StandardButtons,QMessageBox::S"
    "tandardButton,QMessageBox::Icon)\0"
    "t,m,buttons,def\0"
    "messageBox(QString,QString,QMessageBox::StandardButtons,QMessageBox::S"
    "tandardButton)\0"
    "t,m,buttons\0"
    "messageBox(QString,QString,QMessageBox::StandardButtons)\0"
    "QByteArray\0"
    "messageBox(QString,QString,QByteArray,QByteArray,QByteArray)\0"
    "messageBox(QString,QString,QByteArray,QByteArray)\0"
    "messageBox(QString,QString,QByteArray)\0"
    "t,m\0messageBox(QString,QString)\0doNew()\0"
    "doOpen()\0doClose()\0doQuit()\0"
    "doReturnToConsole()\0doReturnToPrevious()\0"
    "doLuaStop()\0doLuaPause()\0doPreferences()\0"
    "doHelp()\0QObjectList\0windows()\0"
    "QStringList\0windowNames()\0recentFiles()\0"
    "QWidget*\0previousWindow()\0activeWindow()\0"
    "sdiMain()\0mdiMain()\0QMenuBar*\0"
    "globalMenuBar()\0menu\0defaultMenuBar(QMenuBar*)\0"
    "QAction*\0name\0hasAction(QByteArray)\0"
    "stdAction(QByteArray)\0editOnError\0"
    "mdiDefault\0"
};

void QLuaIde::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaIde *_t = static_cast<QLuaIde *>(_o);
        switch (_id) {
        case 0: _t->windowsChanged(); break;
        case 1: _t->prefsRequested((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 2: _t->helpRequested((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 3: { QLuaEditor* _r = _t->editor((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QLuaEditor**>(_a[0]) = _r; }  break;
        case 4: { QLuaEditor* _r = _t->editor();
            if (_a[0]) *reinterpret_cast< QLuaEditor**>(_a[0]) = _r; }  break;
        case 5: { QLuaBrowser* _r = _t->browser((*reinterpret_cast< QUrl(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QLuaBrowser**>(_a[0]) = _r; }  break;
        case 6: { QLuaBrowser* _r = _t->browser();
            if (_a[0]) *reinterpret_cast< QLuaBrowser**>(_a[0]) = _r; }  break;
        case 7: { QLuaBrowser* _r = _t->browser((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QLuaBrowser**>(_a[0]) = _r; }  break;
        case 8: { QLuaInspector* _r = _t->inspector();
            if (_a[0]) *reinterpret_cast< QLuaInspector**>(_a[0]) = _r; }  break;
        case 9: { QLuaSdiMain* _r = _t->createSdiMain();
            if (_a[0]) *reinterpret_cast< QLuaSdiMain**>(_a[0]) = _r; }  break;
        case 10: { QLuaMdiMain* _r = _t->createMdiMain();
            if (_a[0]) *reinterpret_cast< QLuaMdiMain**>(_a[0]) = _r; }  break;
        case 11: _t->setEditOnError((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->addRecentFile((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 13: _t->clearRecentFiles(); break;
        case 14: _t->loadRecentFiles(); break;
        case 15: _t->saveRecentFiles(); break;
        case 16: _t->activateWidget((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 17: _t->activateConsole((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 18: _t->activateConsole(); break;
        case 19: _t->loadWindowGeometry((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 20: _t->saveWindowGeometry((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 21: _t->updateActions(); break;
        case 22: { bool _r = _t->openFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< QWidget*(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 23: { bool _r = _t->openFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 24: { bool _r = _t->openFile((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 25: { bool _r = _t->newDocument((*reinterpret_cast< QWidget*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 26: { bool _r = _t->newDocument();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 27: { bool _r = _t->luaExecute((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 28: { bool _r = _t->luaRestart((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 29: { bool _r = _t->quit((*reinterpret_cast< QWidget*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 30: { int _r = _t->messageBox((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QMessageBox::StandardButtons(*)>(_a[3])),(*reinterpret_cast< QMessageBox::StandardButton(*)>(_a[4])),(*reinterpret_cast< QMessageBox::Icon(*)>(_a[5])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 31: { int _r = _t->messageBox((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QMessageBox::StandardButtons(*)>(_a[3])),(*reinterpret_cast< QMessageBox::StandardButton(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 32: { int _r = _t->messageBox((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QMessageBox::StandardButtons(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 33: { QByteArray _r = _t->messageBox((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QByteArray(*)>(_a[3])),(*reinterpret_cast< QByteArray(*)>(_a[4])),(*reinterpret_cast< QByteArray(*)>(_a[5])));
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = _r; }  break;
        case 34: { QByteArray _r = _t->messageBox((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QByteArray(*)>(_a[3])),(*reinterpret_cast< QByteArray(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = _r; }  break;
        case 35: { QByteArray _r = _t->messageBox((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QByteArray(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = _r; }  break;
        case 36: { QByteArray _r = _t->messageBox((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = _r; }  break;
        case 37: _t->doNew(); break;
        case 38: _t->doOpen(); break;
        case 39: { bool _r = _t->doClose();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 40: { bool _r = _t->doQuit();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 41: _t->doReturnToConsole(); break;
        case 42: _t->doReturnToPrevious(); break;
        case 43: _t->doLuaStop(); break;
        case 44: _t->doLuaPause(); break;
        case 45: _t->doPreferences(); break;
        case 46: _t->doHelp(); break;
        case 47: { QObjectList _r = _t->windows();
            if (_a[0]) *reinterpret_cast< QObjectList*>(_a[0]) = _r; }  break;
        case 48: { QStringList _r = _t->windowNames();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = _r; }  break;
        case 49: { QStringList _r = _t->recentFiles();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = _r; }  break;
        case 50: { QWidget* _r = _t->previousWindow();
            if (_a[0]) *reinterpret_cast< QWidget**>(_a[0]) = _r; }  break;
        case 51: { QWidget* _r = _t->activeWindow();
            if (_a[0]) *reinterpret_cast< QWidget**>(_a[0]) = _r; }  break;
        case 52: { QLuaSdiMain* _r = _t->sdiMain();
            if (_a[0]) *reinterpret_cast< QLuaSdiMain**>(_a[0]) = _r; }  break;
        case 53: { QLuaMdiMain* _r = _t->mdiMain();
            if (_a[0]) *reinterpret_cast< QLuaMdiMain**>(_a[0]) = _r; }  break;
        case 54: { QMenuBar* _r = _t->globalMenuBar();
            if (_a[0]) *reinterpret_cast< QMenuBar**>(_a[0]) = _r; }  break;
        case 55: { QMenuBar* _r = _t->defaultMenuBar((*reinterpret_cast< QMenuBar*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QMenuBar**>(_a[0]) = _r; }  break;
        case 56: { QAction* _r = _t->hasAction((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QAction**>(_a[0]) = _r; }  break;
        case 57: { QAction* _r = _t->stdAction((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QAction**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaIde::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaIde::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QLuaIde,
      qt_meta_data_QLuaIde, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaIde::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaIde::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaIde::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaIde))
        return static_cast<void*>(const_cast< QLuaIde*>(this));
    return QObject::qt_metacast(_clname);
}

int QLuaIde::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 58)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 58;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = editOnError(); break;
        case 1: *reinterpret_cast< bool*>(_v) = mdiDefault(); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setEditOnError(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QLuaIde::windowsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void QLuaIde::prefsRequested(QWidget * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QLuaIde::helpRequested(QWidget * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
