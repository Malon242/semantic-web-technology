/****************************************************************************
** Meta object code from reading C++ file 'qluabrowser.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtide/qluabrowser.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluabrowser.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaBrowser[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      27,   14, // methods
       5,  149, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   13,   12,   12, 0x0a,
      30,   13,   12,   12, 0x0a,
      49,   47,   12,   12, 0x0a,
      76,   71,   12,   12, 0x0a,
     115,   98,   93,   12, 0x0a,
     147,  138,   93,   12, 0x2a,
     165,   12,   93,   12, 0x0a,
     179,   12,   12,   12, 0x0a,
     196,   12,   12,   12, 0x0a,
     212,   12,   12,   12, 0x0a,
     223,   12,   12,   12, 0x0a,
     233,   12,   12,   12, 0x0a,
     242,   12,   12,   12, 0x0a,
     251,   12,   12,   12, 0x0a,
     260,   12,   12,   12, 0x0a,
     269,   12,   12,   12, 0x0a,
     281,   12,   12,   12, 0x0a,
     294,   12,   12,   12, 0x0a,
     303,   12,   12,   12, 0x0a,
     314,   12,   12,   12, 0x0a,
     325,   12,   12,   12, 0x0a,
     337,   12,   12,   12, 0x0a,
     351,   12,   12,   12, 0x0a,
     371,   12,   12,   12, 0x0a,
     390,   12,   12,   12, 0x0a,

 // methods: signature, parameters, type, tag, flags
     416,   12,  406,   12, 0x02,
     433,   12,  423,   12, 0x02,

 // properties: name, type, flags
      13,  440, 0x11095103,
     445,  440, 0x11095103,
     461,  453, 0x0a095103,
     471,  453, 0x0a095001,
      71,  453, 0x0a095103,

       0        // eod
};

static const char qt_meta_stringdata_QLuaBrowser[] = {
    "QLuaBrowser\0\0url\0setUrl(QUrl)\0"
    "setHomeUrl(QUrl)\0s\0setBaseTitle(QString)\0"
    "html\0setHtml(QString)\0bool\0fileName,inOther\0"
    "openFile(QString,bool)\0fileName\0"
    "openFile(QString)\0newDocument()\0"
    "doOpenLocation()\0doOpenBrowser()\0"
    "doSaveAs()\0doPrint()\0doCopy()\0doEdit()\0"
    "doFind()\0doHome()\0doForward()\0"
    "doBackward()\0doStop()\0doReload()\0"
    "doZoomIn()\0doZoomOut()\0doZoomReset()\0"
    "updateWindowTitle()\0updateWindowIcon()\0"
    "updateActions()\0QWebView*\0view()\0"
    "QWebPage*\0page()\0QUrl\0homeUrl\0QString\0"
    "baseTitle\0pageTitle\0"
};

void QLuaBrowser::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaBrowser *_t = static_cast<QLuaBrowser *>(_o);
        switch (_id) {
        case 0: _t->setUrl((*reinterpret_cast< QUrl(*)>(_a[1]))); break;
        case 1: _t->setHomeUrl((*reinterpret_cast< QUrl(*)>(_a[1]))); break;
        case 2: _t->setBaseTitle((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->setHtml((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: { bool _r = _t->openFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: { bool _r = _t->openFile((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->newDocument();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: _t->doOpenLocation(); break;
        case 8: _t->doOpenBrowser(); break;
        case 9: _t->doSaveAs(); break;
        case 10: _t->doPrint(); break;
        case 11: _t->doCopy(); break;
        case 12: _t->doEdit(); break;
        case 13: _t->doFind(); break;
        case 14: _t->doHome(); break;
        case 15: _t->doForward(); break;
        case 16: _t->doBackward(); break;
        case 17: _t->doStop(); break;
        case 18: _t->doReload(); break;
        case 19: _t->doZoomIn(); break;
        case 20: _t->doZoomOut(); break;
        case 21: _t->doZoomReset(); break;
        case 22: _t->updateWindowTitle(); break;
        case 23: _t->updateWindowIcon(); break;
        case 24: _t->updateActions(); break;
        case 25: { QWebView* _r = _t->view();
            if (_a[0]) *reinterpret_cast< QWebView**>(_a[0]) = _r; }  break;
        case 26: { QWebPage* _r = _t->page();
            if (_a[0]) *reinterpret_cast< QWebPage**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaBrowser::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaBrowser::staticMetaObject = {
    { &QLuaMainWindow::staticMetaObject, qt_meta_stringdata_QLuaBrowser,
      qt_meta_data_QLuaBrowser, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaBrowser::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaBrowser::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaBrowser::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaBrowser))
        return static_cast<void*>(const_cast< QLuaBrowser*>(this));
    return QLuaMainWindow::qt_metacast(_clname);
}

int QLuaBrowser::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLuaMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QUrl*>(_v) = url(); break;
        case 1: *reinterpret_cast< QUrl*>(_v) = homeUrl(); break;
        case 2: *reinterpret_cast< QString*>(_v) = baseTitle(); break;
        case 3: *reinterpret_cast< QString*>(_v) = pageTitle(); break;
        case 4: *reinterpret_cast< QString*>(_v) = toHtml(); break;
        }
        _id -= 5;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setUrl(*reinterpret_cast< QUrl*>(_v)); break;
        case 1: setHomeUrl(*reinterpret_cast< QUrl*>(_v)); break;
        case 2: setBaseTitle(*reinterpret_cast< QString*>(_v)); break;
        case 4: setHtml(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 5;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
