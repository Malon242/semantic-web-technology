/****************************************************************************
** Meta object code from reading C++ file 'qluamode.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtide/qluamode.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluamode.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaMode[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,    9,   10,    9, 0x0a,
      23,    9,   10,    9, 0x0a,
      33,    9,   10,    9, 0x0a,
      43,    9,   10,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QLuaMode[] = {
    "QLuaMode\0\0bool\0doTab()\0doEnter()\0"
    "doMatch()\0doBalance()\0"
};

void QLuaMode::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaMode *_t = static_cast<QLuaMode *>(_o);
        switch (_id) {
        case 0: { bool _r = _t->doTab();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 1: { bool _r = _t->doEnter();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: { bool _r = _t->doMatch();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 3: { bool _r = _t->doBalance();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaMode::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaMode::staticMetaObject = {
    { &QLuaTextEditMode::staticMetaObject, qt_meta_stringdata_QLuaMode,
      qt_meta_data_QLuaMode, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaMode::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaMode::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaMode::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaMode))
        return static_cast<void*>(const_cast< QLuaMode*>(this));
    return QLuaTextEditMode::qt_metacast(_clname);
}

int QLuaMode::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLuaTextEditMode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
