/****************************************************************************
** Meta object code from reading C++ file 'qtluaprinter.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../packages/qtwidget/qtluaprinter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qtluaprinter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QtLuaPrinter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
      21,   54, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,

 // methods: signature, parameters, type, tag, flags
      42,   13,   32,   13, 0x02,
      66,   13,   52,   13, 0x02,
      79,   75,   13,   13, 0x02,
     103,   13,   98,   13, 0x02,
     113,   13,   98,   13, 0x02,
     128,  121,   98,   13, 0x02,
     144,   13,   98,   13, 0x22,

 // properties: name, type, flags
     152,   98, 0x01095103,
     162,   98, 0x01095103,
     184,  176, 0x0a095103,
     192,  176, 0x0a095103,
     200,   98, 0x01095103,
     220,   98, 0x01095103,
     245,  241, 0x02095001,
     254,   98, 0x01095103,
     263,   98, 0x01095103,
     273,  241, 0x02095103,
     283,  176, 0x0a095103,
     298,  176, 0x0a095103,
     311,  176, 0x0a095103,
     320,  176, 0x0a095103,
     332,  176, 0x0a095103,
     345,  241, 0x02095103,
     356,  241, 0x02095001,
     369,  363, 0x13095001,
     379,  363, 0x13095001,
     395,  388, 0x16095103,
     405,  176, 0x0a095001,

 // enums: name, flags, count, data

 // enum data: key, value

       0        // eod
};

static const char qt_meta_stringdata_QtLuaPrinter[] = {
    "QtLuaPrinter\0\0closing(QObject*)\0"
    "QPrinter*\0printer()\0QPaintDevice*\0"
    "device()\0f,t\0setFromTo(int,int)\0bool\0"
    "newPage()\0abort()\0parent\0setup(QWidget*)\0"
    "setup()\0colorMode\0collateCopies\0QString\0"
    "creator\0docName\0doubleSidedPrinting\0"
    "fontEmbeddingEnabled\0int\0fromPage\0"
    "fullPage\0landscape\0numCopies\0"
    "outputFileName\0outputFormat\0pageSize\0"
    "printerName\0printProgram\0resolution\0"
    "toPage\0QRect\0paperRect\0pageRect\0QSizeF\0"
    "paperSize\0printerState\0"
};

void QtLuaPrinter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QtLuaPrinter *_t = static_cast<QtLuaPrinter *>(_o);
        switch (_id) {
        case 0: _t->closing((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 1: { QPrinter* _r = _t->printer();
            if (_a[0]) *reinterpret_cast< QPrinter**>(_a[0]) = _r; }  break;
        case 2: { QPaintDevice* _r = _t->device();
            if (_a[0]) *reinterpret_cast< QPaintDevice**>(_a[0]) = _r; }  break;
        case 3: _t->setFromTo((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: { bool _r = _t->newPage();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: { bool _r = _t->abort();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->setup((*reinterpret_cast< QWidget*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->setup();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QtLuaPrinter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QtLuaPrinter::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QtLuaPrinter,
      qt_meta_data_QtLuaPrinter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QtLuaPrinter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QtLuaPrinter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QtLuaPrinter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QtLuaPrinter))
        return static_cast<void*>(const_cast< QtLuaPrinter*>(this));
    if (!strcmp(_clname, "QPrinter"))
        return static_cast< QPrinter*>(const_cast< QtLuaPrinter*>(this));
    return QObject::qt_metacast(_clname);
}

int QtLuaPrinter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = colorMode(); break;
        case 1: *reinterpret_cast< bool*>(_v) = collateCopies(); break;
        case 2: *reinterpret_cast< QString*>(_v) = creator(); break;
        case 3: *reinterpret_cast< QString*>(_v) = docName(); break;
        case 4: *reinterpret_cast< bool*>(_v) = doubleSidedPrinting(); break;
        case 5: *reinterpret_cast< bool*>(_v) = fontEmbeddingEnabled(); break;
        case 6: *reinterpret_cast< int*>(_v) = fromPage(); break;
        case 7: *reinterpret_cast< bool*>(_v) = fullPage(); break;
        case 8: *reinterpret_cast< bool*>(_v) = landscape(); break;
        case 9: *reinterpret_cast< int*>(_v) = numCopies(); break;
        case 10: *reinterpret_cast< QString*>(_v) = outputFileName(); break;
        case 11: *reinterpret_cast< QString*>(_v) = outputFormat(); break;
        case 12: *reinterpret_cast< QString*>(_v) = pageSize(); break;
        case 13: *reinterpret_cast< QString*>(_v) = printerName(); break;
        case 14: *reinterpret_cast< QString*>(_v) = printProgram(); break;
        case 15: *reinterpret_cast< int*>(_v) = resolution(); break;
        case 16: *reinterpret_cast< int*>(_v) = toPage(); break;
        case 17: *reinterpret_cast< QRect*>(_v) = paperRect(); break;
        case 18: *reinterpret_cast< QRect*>(_v) = pageRect(); break;
        case 19: *reinterpret_cast< QSizeF*>(_v) = paperSize(); break;
        case 20: *reinterpret_cast< QString*>(_v) = printerState(); break;
        }
        _id -= 21;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setColorMode(*reinterpret_cast< bool*>(_v)); break;
        case 1: setCollateCopies(*reinterpret_cast< bool*>(_v)); break;
        case 2: setCreator(*reinterpret_cast< QString*>(_v)); break;
        case 3: setDocName(*reinterpret_cast< QString*>(_v)); break;
        case 4: setDoubleSidedPrinting(*reinterpret_cast< bool*>(_v)); break;
        case 5: setFontEmbeddingEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 7: setFullPage(*reinterpret_cast< bool*>(_v)); break;
        case 8: setLandscape(*reinterpret_cast< bool*>(_v)); break;
        case 9: setNumCopies(*reinterpret_cast< int*>(_v)); break;
        case 10: setOutputFileName(*reinterpret_cast< QString*>(_v)); break;
        case 11: setOutputFormat(*reinterpret_cast< QString*>(_v)); break;
        case 12: setPageSize(*reinterpret_cast< QString*>(_v)); break;
        case 13: setPrinterName(*reinterpret_cast< QString*>(_v)); break;
        case 14: setPrintProgram(*reinterpret_cast< QString*>(_v)); break;
        case 15: setResolution(*reinterpret_cast< int*>(_v)); break;
        case 19: setPaperSize(*reinterpret_cast< QSizeF*>(_v)); break;
        }
        _id -= 21;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 21;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QtLuaPrinter::closing(QObject * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
