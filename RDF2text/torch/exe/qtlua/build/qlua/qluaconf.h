/* -*- C++ -*- */

#ifndef QLUACONF_H
#define QLUACONF_H

#ifdef WIN32
# ifdef libqlua_EXPORTS
#  define QLUAAPI __declspec(dllexport)
# else
#  define QLUAAPI __declspec(dllimport)
# endif
#else
# define QLUAAPI /**/
#endif

#define QLUA_VERSION "0.1"

/* #undef HAVE_IO_H */
/* #undef HAVE_CGETS_S */

#define HAVE_FCNTL_H 1
#define HAVE_PTHREAD_H 1
#define HAVE_SIGNAL_H 1
#define HAVE_UNISTD_H 1
#define HAVE_ERRNO_H 1
#define HAVE_SYS_SELECT_H 1
#define HAVE_SYS_TIME_H 1
#define HAVE_SYS_TYPES_H 1
#define HAVE_ISATTY 1
/* #undef HAVE_PTHREAD_SIGMASK */
#define HAVE_RL_COMPLETION_MATCHES 1
#define HAVE_SIGACTION 1
#define HAVE_SIGPROCMASK 1
#define HAVE_READLINE 1
/* #undef HAVE_XINITTHREADS */

#endif
