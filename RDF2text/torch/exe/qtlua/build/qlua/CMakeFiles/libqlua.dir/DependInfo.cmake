# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/data/s1531565/torch/exe/qtlua/build/qlua/moc_qluaapplication.cxx" "/data/s1531565/torch/exe/qtlua/build/qlua/CMakeFiles/libqlua.dir/moc_qluaapplication.cxx.o"
  "/data/s1531565/torch/exe/qtlua/build/qlua/moc_qluaconsole.cxx" "/data/s1531565/torch/exe/qtlua/build/qlua/CMakeFiles/libqlua.dir/moc_qluaconsole.cxx.o"
  "/data/s1531565/torch/exe/qtlua/qlua/qluaapplication.cpp" "/data/s1531565/torch/exe/qtlua/build/qlua/CMakeFiles/libqlua.dir/qluaapplication.cpp.o"
  "/data/s1531565/torch/exe/qtlua/qlua/qluaconsole_unix.cpp" "/data/s1531565/torch/exe/qtlua/build/qlua/CMakeFiles/libqlua.dir/qluaconsole_unix.cpp.o"
  "/data/s1531565/torch/exe/qtlua/build/qlua/qrc_qlua.cxx" "/data/s1531565/torch/exe/qtlua/build/qlua/CMakeFiles/libqlua.dir/qrc_qlua.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/data/s1531565/torch/install/include"
  "../qlua"
  "qlua"
  "../qtlua"
  "qtlua"
  "/usr/include/QtGui"
  "/usr/include/QtCore"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/data/s1531565/torch/exe/qtlua/build/qtlua/CMakeFiles/libqtlua.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
