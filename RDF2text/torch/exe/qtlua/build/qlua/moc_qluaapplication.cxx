/****************************************************************************
** Meta object code from reading C++ file 'qluaapplication.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qlua/qluaapplication.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qluaapplication.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QLuaApplication[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       1,  109, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: signature, parameters, type, tag, flags
      24,   17,   16,   16, 0x05,
      48,   16,   16,   16, 0x05,
      84,   72,   16,   16, 0x05,
     137,  133,   16,   16, 0x05,
     172,  166,   16,   16, 0x05,
     206,   16,   16,   16, 0x05,
     229,  222,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
     251,   16,   16,   16, 0x0a,
     276,  272,   16,   16, 0x0a,
     310,  306,  301,   16, 0x0a,
     345,  333,   16,   16, 0x0a,
     359,   16,   16,   16, 0x2a,
     369,   16,  301,   16, 0x0a,
     377,   16,   16,   16, 0x0a,

 // methods: signature, parameters, type, tag, flags
     392,   16,  385,   16, 0x02,
     426,  422,  413,   16, 0x02,
     458,  448,   16,   16, 0x02,
     502,   16,  490,   16, 0x02,
     516,   16,  301,   16, 0x02,

 // properties: name, type, flags
     546,  538, 0x0a095103,

       0        // eod
};

static const char qt_meta_stringdata_QLuaApplication[] = {
    "QLuaApplication\0\0engine\0newEngine(QtLuaEngine*)\0"
    "acceptingCommands(bool)\0ps1,ps2,cmd\0"
    "luaCommandEcho(QByteArray,QByteArray,QByteArray)\0"
    "out\0luaConsoleOutput(QByteArray)\0pbool\0"
    "anyoneHandlesConsoleOutput(bool&)\0"
    "fileOpenEvent()\0window\0windowShown(QWidget*)\0"
    "setupConsoleOutput()\0msg\0"
    "setAboutMessage(QString)\0bool\0cmd\0"
    "runCommand(QByteArray)\0redoCmdLine\0"
    "restart(bool)\0restart()\0close()\0about()\0"
    "double\0timeForLastCommand()\0QVariant\0"
    "key\0readSettings(QString)\0key,value\0"
    "writeSettings(QString,QVariant)\0"
    "QStringList\0filesToOpen()\0"
    "runsWithoutGraphics()\0QString\0"
    "aboutMessage\0"
};

void QLuaApplication::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QLuaApplication *_t = static_cast<QLuaApplication *>(_o);
        switch (_id) {
        case 0: _t->newEngine((*reinterpret_cast< QtLuaEngine*(*)>(_a[1]))); break;
        case 1: _t->acceptingCommands((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->luaCommandEcho((*reinterpret_cast< QByteArray(*)>(_a[1])),(*reinterpret_cast< QByteArray(*)>(_a[2])),(*reinterpret_cast< QByteArray(*)>(_a[3]))); break;
        case 3: _t->luaConsoleOutput((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 4: _t->anyoneHandlesConsoleOutput((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->fileOpenEvent(); break;
        case 6: _t->windowShown((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 7: _t->setupConsoleOutput(); break;
        case 8: _t->setAboutMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: { bool _r = _t->runCommand((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 10: _t->restart((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->restart(); break;
        case 12: { bool _r = _t->close();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: _t->about(); break;
        case 14: { double _r = _t->timeForLastCommand();
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = _r; }  break;
        case 15: { QVariant _r = _t->readSettings((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QVariant*>(_a[0]) = _r; }  break;
        case 16: _t->writeSettings((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QVariant(*)>(_a[2]))); break;
        case 17: { QStringList _r = _t->filesToOpen();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = _r; }  break;
        case 18: { bool _r = _t->runsWithoutGraphics();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QLuaApplication::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QLuaApplication::staticMetaObject = {
    { &QApplication::staticMetaObject, qt_meta_stringdata_QLuaApplication,
      qt_meta_data_QLuaApplication, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QLuaApplication::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QLuaApplication::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QLuaApplication::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QLuaApplication))
        return static_cast<void*>(const_cast< QLuaApplication*>(this));
    return QApplication::qt_metacast(_clname);
}

int QLuaApplication::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QApplication::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = aboutMessage(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setAboutMessage(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QLuaApplication::newEngine(QtLuaEngine * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QLuaApplication::acceptingCommands(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QLuaApplication::luaCommandEcho(QByteArray _t1, QByteArray _t2, QByteArray _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QLuaApplication::luaConsoleOutput(QByteArray _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QLuaApplication::anyoneHandlesConsoleOutput(bool & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QLuaApplication::fileOpenEvent()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void QLuaApplication::windowShown(QWidget * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_END_MOC_NAMESPACE
