#!/bin/bash
#SBATCH --time=12:00:00
#SBATCH --partition=gpu
#SBATCH --gres=gpu:1
#SBATCH --mem=64000
#SBATCH --mail-user=a.j.van.eerden@student.rug.nl

module load cuDNN/5.0-CUDA-7.5.18
module load foss/2016a

# This needs to be after loading the modules
./torch/install/bin/torch-activate

echo Just a small test to check whether gpu is working
th gpuTest.lua

# THERE ARE TWO VARIANTS OF CONVERTING THE XML DATA, FLAT OR STRUCTURED, I USED FLAT

echo 0. Generating datasets
mkdir ./datasets
python ./source/generate_train_dataset.py   -path ./webnlg_challenge_2017-from-webnlg-dataset/train   -src_mode  flat       -src ./datasets/flat/train.src       -tgt ./datasets/flat/train.tgt
#python ./source/generate_train_dataset.py   -path ./webnlg_challenge_2017-from-webnlg-dataset/train   -src_mode  structured -src ./datasets/structured/train.src -tgt ./datasets/structured/train.tgt

python ./source/generate_eval_dataset.py   -path ./webnlg_challenge_2017-from-webnlg-dataset/dev   -src_mode  flat          -src ./datasets/flat/dev.src         -tgt ./datasets/flat/dev.tgt       -ref ./datasets/flat/dev.ref       -relex ./datasets/flat/dev.relex
#python ./source/generate_eval_dataset.py   -path ./webnlg_challenge_2017-from-webnlg-dataset/dev   -src_mode  structured    -src ./datasets/structured/dev.src   -tgt ./datasets/structured/dev.tgt -ref ./datasets/structured/dev.ref -relex ./datasets/structured/dev.relex


cd ./OpenNMT
echo 1. PREPROCESS

th preprocess.lua -train_src ../datasets/flat/train.src -train_tgt ../datasets/flat/train.tgt -valid_src ../datasets/flat/dev.src -valid_tgt ../datasets/flat/dev.tgt -save_data ../datasets/flat/data_tensor
#th preprocess.lua -train_src ../datasets/structured/train.src -train_tgt ../datasets/structured/train.tgt -valid_src ../datasets/structured/dev.src -valid_tgt ../datasets/structured/dev.tgt -save_data ../datasets/structured/data_tensor


echo 2. TRAIN A MODEL
th train.lua -data ../datasets/flat/data_tensor-train.t7 -save_model ../datasets/flat/s2s_model
#th train.lua -data ../datasets/structured/data_tensor-train.t7 -save_model ../datasets/structured/s2s_model


echo 3. USE THE MODEL for inference.
th translate.lua -model ../datasets/flat/s2s_model_FILENAMEOFLASTEPOCHHERE -src ../datasets/flat/dev.src  -output ../datasets/flat/predictions.dev
#th translate.lua -model ../datasets/structured/s2s_model_epoch13_5.86.t7 -src ../datasets/structured/dev.src  -output ../datasets/structured/predictions.dev


echo 4. RELEXICALIZE PREDICTIONS
cd ..
python3 source/relex_predictions.py -pred ./datasets/flat/predictions.dev       -relex ./datasets/flat/dev.relex       -output ./datasets/flat/predictions.relex
#python3 source/relex_predictions.py -pred ./datasets/structured/predictions.dev -relex ./datasets/structured/dev.relex -output ./datasets/structured/predictions.relex


echo 5. EVALUATE with BLEU script.
#./source/multi-bleu.perl  ./datasets/flat/dev.ref1 < ./datasets/flat/predictions.relex
python3 ./calculate-bleu.py --reference ./datasets/flat/dev.ref1 --candidate ./datasets/flat/predictions.relex

#./source/multi-bleu.perl  ./datasets/structured/dev.ref1 < ./datasets/structured/predictions.relex
#python3 ./calculate-bleu.py --reference ./datasets/structured/dev.ref1 --candidate ./datasets/structured/predictions.relex
