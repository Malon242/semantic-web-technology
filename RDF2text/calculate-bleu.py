from nltk.translate.bleu_score import sentence_bleu
import argparse

# USE: python file_name.py --reference file1.txt --candidate file2.txt
# FROM: https://stackoverflow.com/a/49886758

def argparser():
    Argparser = argparse.ArgumentParser()
    Argparser.add_argument('--reference', type=str, default='summaries.txt', help='Reference File')
    Argparser.add_argument('--candidate', type=str, default='candidates.txt', help='Candidate file')

    args = Argparser.parse_args()
    return args

args = argparser()

reference = open(args.reference, 'r', encoding='utf-8').readlines()
candidate = open(args.candidate, 'r', encoding='utf-8').readlines()

if len(reference) != len(candidate):
    raise ValueError('The number of sentences in both files do not match.')

#from nltk.translate.bleu_score import sentence_bleu
print("TEST SENTENCE")
ref = [['Elisabeth', 'II', 'is', 'a', 'leader', 'in', 'the', 'United', 'Kingdom', '.']]
cand = ['Elisabeth', 'II', 'is', 'the', 'leader', 'of', 'United', 'Kingdom', '.']
score = sentence_bleu(ref, cand, weights=(0.25, 0.25, 0.25, 0.25))
print("BLUE-4", score)
score = sentence_bleu(ref, cand, weights=(1, 0, 0, 0))
print("BLUE-1", score)


PRINTEXTRA = False
totaalscore = 0.
aantal = len(reference)

for i in range(len(reference)):
    score = sentence_bleu([reference[i].strip().split()], candidate[i].strip().split(), weights=(1., 0., 0., 0., 0.))
    #REMOVING THE 0 scores
    #if score < 0.0001:
    #    aantal -= 1
    #else:
    #    totaalscore += score
    totaalscore += score

    if PRINTEXTRA:
        print("sentence", i)
        print(reference[i].strip().split())
        print(candidate[i].strip().split())
        print(score)
        print('\n')

print(aantal)
totaalscore /= aantal
print("The bleu-1 score is: "+str(totaalscore))

#BLEU-4 0.15381529584426712
# 148/452 goed
# 0.4697602278487077
# 0.6041693748901236
